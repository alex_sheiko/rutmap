#include <mbgl/util/platform.hpp>

#define PACKAGE_NAME "com.maptiler.geoeditor"
#define FILES_DIR "/data/data/com.maptiler.geoeditor/files/tiles/"

namespace mbgl {
namespace platform {
const std::string schemeSeparator = "://";
const std::string extension = ".mbtiles";

std::string getFullPathToDatabase(const std::string& uri) {
    // Remove 'maptiler://''
    std::string path;
    auto schemePos = uri.find(schemeSeparator);
    // If uri contains scheme , remove it
    if (schemePos != std::string::npos) {
        path = uri.substr(schemePos + schemeSeparator.length());
    } else {
        path = uri;
    }
    // Remove everything after mbtiles extension
    auto pos = path.find(extension);
    if (pos != std::string::npos) {
        path = path.substr(0, pos + extension.size());
    }
    // If the path is absolute, return just path, else prepend tile directory
    return (path[0] == '/' ? "" : FILES_DIR) + path;
}

} // platform
} // mbgl
