package rutmap.ba.rutmap.ui.streetlist

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.animation.OvershootInterpolator
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_event_list.*
import kotlinx.android.synthetic.main.include_header_street_list.*
import kotlinx.android.synthetic.main.include_toolbar_street_list.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.util.design.image.showImage

class StreetListActivity : BaseActivityWithSearch() {

    val mPresenter by lazy { StreetListPresenter(this) }
    val mAdapter by lazy { StreetListAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_street_list)

        setupActionBar()
        setupRecyclerView()

        showData()
    }

    fun populateList(streets: List<Street>) {
        mAdapter.addAll(streets)
        mAdapter.notifyItemRangeInserted(0, streets.size)
    }

    fun showCount(size: Int) {
        textCount.text = textCount.text.toString().replace("1374", "$size")
    }

    private fun showData() {
        mPresenter.showStreets()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showHeaderImage()
    }

    private fun showHeaderImage() {
        imageHeader.showImage(this,
                R.drawable.bg_header_streets)
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mAdapter
        recyclerView.itemAnimator = SlideInUpAnimator(
                OvershootInterpolator(1f))
    }
}