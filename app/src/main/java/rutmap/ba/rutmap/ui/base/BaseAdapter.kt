package rutmap.ba.rutmap.ui.base

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import java.util.*

abstract class BaseAdapter<T : RecyclerView.ViewHolder>
    : Adapter<T>() {

    val dataset = ArrayList<Any>()

    override fun getItemCount(): Int {
        return dataset.size
    }

    fun addAll(items: List<Any>) {
        dataset.addAll(items)
    }

    fun clear() {
        dataset.clear()
    }
}