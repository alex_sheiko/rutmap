package rutmap.ba.rutmap.ui.guidelist

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.animation.OvershootInterpolator
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_event_list.*
import kotlinx.android.synthetic.main.include_toolbar_guide_list.*
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.util.common.showAd
import rutmap.ba.rutmap.util.data.getGuideList
import rutmap.ba.rutmap.util.design.image.showImage

class GuideListActivity : BaseActivityWithSearch() {

    val mAdapter by lazy { GuideListAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide_list)

        setupActionBar()
        setupRecyclerView()

        showData()
        setClickListeners()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mAdapter
        recyclerView.itemAnimator = SlideInUpAnimator(
                OvershootInterpolator(1f))
    }

    private fun showData() {
        mAdapter.addAll(getGuideList())

        imageAd.showImage(this, R.drawable.ad_banner_large)
    }

    private fun setClickListeners() {
        imageAd.onClick { showAd() }
    }
}