package rutmap.ba.rutmap.ui.placelist.map

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.fragment_map.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseMapFragment
import rutmap.ba.rutmap.util.common.getCategoryExtra
import rutmap.ba.rutmap.util.data.getSavedLocation
import rutmap.ba.rutmap.util.data.isMyLocationEnabled
import rutmap.ba.rutmap.util.map.offline.OMTMapView
import rutmap.ba.rutmap.util.map.offline.Style
import rutmap.ba.rutmap.util.map.showMarkersFamous
import rutmap.ba.rutmap.util.map.showMarkersPlaces

@SuppressLint("ValidFragment")
class PlaceListMapFragment(
        var isOnlyPremium: Boolean = false,
        var isRecommended: Boolean = false)
    : BaseMapFragment() {

    lateinit override var mMapView: OMTMapView
    lateinit override var mMap: MapboxMap
    lateinit var mLocationHint: ImageView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_map,
                container,
                false)
        mLocationHint = view.locationHint

        mMapView = view.mapView
        mMapView.style = Style.createFromAsset(context, "bright.json")

        mMapView.onCreate(savedInstanceState)
        mMapView.getMapAsync {
            mMap = it
            mMap.uiSettings.isLogoEnabled = false
            mMap.uiSettings.setAttributionMargins(0, 0, 0, 0)
            mMap.showMarkersFamous(context, callback = {
                if (!isOnlyPremium && context.getSavedLocation() == null) {
                    isOnlyPremium = true
                }
                val isAd = activity.intent.hasExtra("ad")
                mMap.showMarkersPlaces(context,
                        getCategoryExtra(),
                        isOnlyPremium,
                        isRecommended,
                        isAd,
                        {
                            val locationList = it
                            if (isMyLocationEnabled()) {
                                mMap.loadMyLocation(locationList, isAd)
                            }
                        })
            })
        }
        view.setMyLocationClickListener()

        return view
    }

    override fun onStart() {
        super.onStart()
        mMapView.reapplyStyle()
    }

    override fun showLocationPopup() {
        mLocationHint.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed({
            mLocationHint.visibility = View.GONE
        }, 10000)
    }
}