package rutmap.ba.rutmap.ui.placelist

import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.fragment_place_list.view.*
import kotlinx.android.synthetic.main.include_header_place_list.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.ui.placelist.header.PlacesPremiumFragment
import rutmap.ba.rutmap.util.common.getCategoryExtra
import rutmap.ba.rutmap.util.data.getPremiumPlaces
import rutmap.ba.rutmap.util.map.getLocation

class PlaceListFragment : Fragment() {

    val mActivity by lazy { activity as PlaceListActivity }
    val mPresenter by lazy { PlaceListPresenter(this) }
    val mAdapter by lazy { PlaceListAdapter(activity) }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_place_list,
                container,
                false)

        val category = mActivity.getCategoryExtra()
        view.setupRecyclerView()
        view.showHeaderPremium()
        showData(category)

        return view
    }

    fun populateList(places: List<Any>, myLocation: Location?) {
        mAdapter.addAll(places)
        mAdapter.myLocation = myLocation
        mAdapter.notifyDataSetChanged()
    }

    private fun View.showHeaderPremium() {
        val category = mActivity.getCategoryExtra() ?: return
        val placeListPremium = category.getPremiumPlaces()

        if (placeListPremium.isNotEmpty()) {
            pagerPremium.visibility = VISIBLE
            pagerPremium.adapter = SectionsPagerAdapter(
                    activity.supportFragmentManager,
                    placeListPremium)
        }
    }

    private fun View.setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(mActivity)
        recyclerView.adapter = mAdapter
        recyclerView.itemAnimator = SlideInUpAnimator(
                OvershootInterpolator(1f))
    }

    private fun showData(category: Category?) {
        getLocation {
            val myLocation = it
            mPresenter.showPlaces(category, myLocation)
        }
    }

    inner class SectionsPagerAdapter
    constructor(fm: FragmentManager, val dataset: List<Int>) :
            FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            val placeId = dataset[position]
            return PlacesPremiumFragment(placeId)
        }

        override fun getCount(): Int {
            return dataset.size
        }
    }
}