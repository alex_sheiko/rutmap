package rutmap.ba.rutmap.ui.search

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import io.reactivex.Observable.merge
import kotlinx.android.synthetic.main.activity_search.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.ui.base.BaseActivity
import rutmap.ba.rutmap.util.common.async

class SearchActivity : BaseActivity() {

    val mAdapter by lazy { SearchAdapter(this) }
    val mHandler by lazy { Handler() }
    var mAdapterQuery: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        setupRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchView.onActionViewExpanded()
        searchView.setQueryListener()

        return true
    }

    override fun onBackPressed() {
        overridePendingTransition(0, 0)
        super.onBackPressed()
    }

    private fun SearchView.setQueryListener() {
        setOnQueryTextListener(
                object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String): Boolean {
                        return false
                    }

                    override fun onQueryTextChange(query: String): Boolean {
                        mAdapterQuery = query

                        mHandler.removeCallbacksAndMessages(null)
                        mHandler.postDelayed({

                            search(query, {
                                mAdapter.clear()
                                mAdapter.addAll(it)
                                mAdapter.notifyDataSetChanged()
                            })
                        }, 300)

                        return true
                    }
                })
    }

    private fun setupRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = mAdapter
    }

    var mIndex = 0

    private fun search(input: String, callback: (List<Any>) -> Unit) {
        if (input.length < 2) {
            callback.invoke(emptyList())
            return
        }
        val manager = DataManager(this)

        val placesStream = manager.searchPlaces(input)
        val streetsStream = manager.searchStreets(input)
        val eventsStream = manager.searchEvents(input)

        val resultList = ArrayList<Any>()

        merge(placesStream, streetsStream, eventsStream)
                .async()
                .subscribe {
                    if (mIndex == 2) {
                        callback.invoke(resultList)
                        mIndex = 0
                    } else {
                        resultList.addAll(it)
                        mIndex++
                    }
                }
    }
}