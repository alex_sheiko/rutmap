package rutmap.ba.rutmap.ui.placelist

import android.app.Activity
import android.location.Location
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers.computation
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.util.common.sortByDistance

class PlaceListPresenter(val view: PlaceListFragment) {

    val mDataManager = DataManager(view.activity)

    fun showPlaces(category: Category?, myLocation: Location?) {
        val isAd = (view.context as Activity).intent.hasExtra("ad")

        mDataManager.loadPlaces(category, isAd = isAd)
                .subscribeOn(computation())
                .observeOn(mainThread())
                .subscribe({
                    if (myLocation != null) {
                        it.sortByDistance(myLocation)
                    }
                    view.populateList(it, myLocation)
                })
    }
}