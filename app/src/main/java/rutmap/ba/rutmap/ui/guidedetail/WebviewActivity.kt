package rutmap.ba.rutmap.ui.guidedetail

import android.os.Bundle
import android.view.View
import android.webkit.WebViewClient
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_webview.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivity
import rutmap.ba.rutmap.util.common.hasNetwork

class WebviewActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)

        if (hasNetwork()) {
            showUrl(getUrl())
        } else {
            showPlaceholder()
        }
    }

    private fun showUrl(url: String?) {
        webView.setWebViewClient(WebViewClient())
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(url)
    }

    private fun getUrl(): String? {
        return intent.getStringExtra("url")
    }

    private fun showPlaceholder() {
        containerPlaceholder.visibility = View.VISIBLE

        Glide.with(this)
                .load(R.drawable.placeholder_webview)
                .into(imagePlaceholder)
    }
}
