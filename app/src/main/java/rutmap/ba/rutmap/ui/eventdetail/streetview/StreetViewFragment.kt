package rutmap.ba.rutmap.ui.eventdetail.streetview

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_street_view.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.util.common.eventMode
import rutmap.ba.rutmap.util.common.getEventExtra
import rutmap.ba.rutmap.util.common.getPlaceExtra
import rutmap.ba.rutmap.util.common.hasNetwork
import rutmap.ba.rutmap.util.data.model.loadPlaceAsync
import rutmap.ba.rutmap.util.data.model.showStreetView

class StreetViewFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_street_view,
                container,
                false)

        if (hasNetwork()) {
            view.setupWebView()
            view.showStreetView()
        } else {
            view.showPlaceholder()
        }

        return view
    }

    private fun View.setupWebView() {
        webView.settings.javaScriptEnabled = true
    }

    private fun View.showStreetView() {
        loadPlace({
            webView.showStreetView(it)
        })
    }

    private fun loadPlace(callback: (Place) -> Unit) {
        if (eventMode()) {
            val event = getEventExtra()
            event.loadPlaceAsync(activity, {
                callback.invoke(it)
            })
        } else {
            callback.invoke(getPlaceExtra()!!)
        }
    }

    private fun View.showPlaceholder() {
        containerPlaceholder.visibility = VISIBLE

        Glide.with(context)
                .load(R.drawable.placeholder_streetview)
                .into(imagePlaceholder)
    }
}
