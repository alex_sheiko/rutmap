package rutmap.ba.rutmap.ui.base

import android.view.Menu
import android.view.MenuItem
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.search.SearchActivity

abstract class BaseActivityWithSearch : BaseActivity() {

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_link, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_search) {
            startActivity<SearchActivity>()
            overridePendingTransition(0, 0)
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}