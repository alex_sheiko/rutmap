package rutmap.ba.rutmap.ui.search

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.ui.base.BaseAdapter
import rutmap.ba.rutmap.ui.search.SearchAdapter.ViewHolder
import rutmap.ba.rutmap.util.navigateToDetails
import rutmap.ba.rutmap.util.showDetails

class SearchAdapter(val ctx: Context)
    : BaseAdapter<ViewHolder>() {

    val TYPE_PLACE = 1
    val TYPE_STREET = 2
    val TYPE_EVENT = 3

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = from(parent.context)
                .inflate(R.layout.item_search, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataset[position]

        showData(holder, item)

        setClickListener(holder, item)
    }

    override fun getItemViewType(position: Int): Int {
        val item = dataset[position]
        if (item is Place) {
            return TYPE_PLACE
        } else if (item is Street) {
            return TYPE_STREET
        } else if (item is Event) {
            return TYPE_EVENT
        }
        return super.getItemViewType(position)
    }

    private fun setClickListener(holder: ViewHolder, item: Any) {
        holder.itemView.onClick {
            item.navigateToDetails(ctx)
        }
    }

    private fun showData(holder: ViewHolder, item: Any) {
        val activity = ctx as SearchActivity
        val query = activity.mAdapterQuery

        holder.showDetails(ctx, item, query)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val textTitle: TextView = v.find<TextView>(R.id.textTitle)
        val textDesc: TextView = v.find<TextView>(R.id.textDesc)
        val textTags: TextView = v.find<TextView>(R.id.textTags)
        val imageIcon: ImageView = v.find<ImageView>(R.id.imageIcon)
    }
}