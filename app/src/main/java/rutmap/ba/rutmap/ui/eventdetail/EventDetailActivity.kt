package rutmap.ba.rutmap.ui.eventdetail

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.include_tabs_detail.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.ui.eventdetail.streetview.StreetViewFragment
import rutmap.ba.rutmap.ui.placedetail.PlaceMapFragment

class EventDetailActivity : BaseActivityWithSearch() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)

        setupPages()
    }

    private fun setupPages() {
        pager.adapter = SectionsPagerAdapter(supportFragmentManager)

        pager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(
                TabLayout.ViewPagerOnTabSelectedListener(pager))
    }

    inner class SectionsPagerAdapter
    constructor(fm: FragmentManager) :
            FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                1 -> return PlaceMapFragment()
                2 -> return StreetViewFragment()
                else -> return LocationDetailFragment()
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }
}