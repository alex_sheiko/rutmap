package rutmap.ba.rutmap.ui.streetlist

import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers.computation
import rutmap.ba.rutmap.data.DataManager

class StreetListPresenter(val view: StreetListActivity) {

    val mDataManager = DataManager(view)

    fun showStreets() {
        mDataManager.loadStreets()
                .subscribeOn(computation())
                .observeOn(mainThread())
                .subscribe({
                    view.showCount(it.size)
                    view.populateList(it)
                })
    }
}