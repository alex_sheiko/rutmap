package rutmap.ba.rutmap.ui.streetlist

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_street.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.ui.base.BaseAdapter
import rutmap.ba.rutmap.ui.streetdetail.StreetMapActivity
import rutmap.ba.rutmap.ui.streetlist.StreetListAdapter.ViewHolder
import rutmap.ba.rutmap.util.data.setBookmarkListener

class StreetListAdapter(val ctx: Context)
    : BaseAdapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = from(parent.context)
                .inflate(R.layout.item_street, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val street = dataset[position] as Street

        holder.showData(street)

        holder.setClickListener(street)
    }

    private fun ViewHolder.setClickListener(street: Street) {
        itemView.onClick {
            val activity = ctx as Activity

            activity.startActivity<StreetMapActivity>(
                    "street" to street
            )
        }
        itemView.setBookmarkListener(street.id, "street")
    }

    private fun ViewHolder.showData(street: Street) {
        textName.text = street.name
        textMunicipality.text = street.municipality_id
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val textName: TextView = v.textName
        val textMunicipality: TextView = v.textMunicipality
    }
}