package rutmap.ba.rutmap.ui.base

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.view.View
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newLatLngZoom
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.fragment_map.view.*
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.BuildConfig.DEBUG
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.placelist.PlaceListActivity
import rutmap.ba.rutmap.util.common.restart
import rutmap.ba.rutmap.util.data.getLocale
import rutmap.ba.rutmap.util.data.isMyLocationEnabled
import rutmap.ba.rutmap.util.data.saveLocation
import rutmap.ba.rutmap.util.data.saveMyLocationEnabled
import rutmap.ba.rutmap.util.map.calculateBounds
import rutmap.ba.rutmap.util.map.moveToBounds
import rutmap.ba.rutmap.util.map.offline.OMTMapView

abstract class BaseMapFragment : Fragment() {

    abstract var mMapView: OMTMapView
    abstract var mMap: MapboxMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mapbox.getInstance(activity,
                getString(R.string.mapbox_access_token))
    }

    override fun onStart() {
        super.onStart()
        mMapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()

        if (!isMyLocationEnabled()) {
            showLocationPopup()
        }
    }

    abstract fun showLocationPopup()

    override fun onPause() {
        mMapView.onPause()
        super.onPause()
    }

    override fun onStop() {
        mMapView.onStop()
        super.onStop()
    }

    override fun onDestroy() {
        if (DEBUG) {
            try {
                mMapView.onDestroy()
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
        } else {
            mMapView.onDestroy()
        }
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        mMapView.onSaveInstanceState(outState)
        super.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        mMapView.onLowMemory()
        super.onLowMemory()
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {

        if (activity is PlaceListActivity
                && activity.intent.hasExtra("ad")) {
            activity.restart(activity.getLocale())
            activity.overridePendingTransition(
                    android.R.anim.fade_in,
                    android.R.anim.fade_out)
            return
        }

        if (grantResults.isNotEmpty()
                && grantResults[0] == PERMISSION_GRANTED) {
            mMap.forceLocationUpdate()
        }
    }

    fun MapboxMap.loadMyLocation(locationList: ArrayList<Location>? = null, makeBounds: Boolean) {
        if (checkSelfPermission(activity, ACCESS_FINE_LOCATION)
                != PERMISSION_GRANTED) {
            requestPermissions(arrayOf(ACCESS_FINE_LOCATION), 123)
        } else {
            forceLocationUpdate(locationList, makeBounds)
        }
    }

    open fun MapboxMap.forceLocationUpdate(
            locationList: ArrayList<Location>? = null,
            makeBounds: Boolean = false) {
        isMyLocationEnabled = true

        if (myLocation != null) {
            locationList?.add(myLocation!!)

            if (makeBounds) {
                moveToBounds(calculateBounds(locationList!!), context)
            } else {
                moveCamera(newLatLngZoom(LatLng(myLocation), 16.0))
            }
            context?.saveLocation(myLocation!!)
        } else {
            setOnMyLocationChangeListener({
                if (myLocation != null) {
                    locationList?.add(myLocation!!)

                    if (makeBounds) {
                        moveToBounds(calculateBounds(locationList!!), context)
                    } else {
                        moveCamera(newLatLngZoom(LatLng(myLocation!!), 16.0))
                    }
                    context?.saveLocation(myLocation!!)

                    setOnMyLocationChangeListener(null)
                }
            })
        }
    }

    fun View.setMyLocationClickListener() {
        buttonMyLocation.onClick {
            if (isMyLocationEnabled()) {
                mMap.forceLocationUpdate()
            } else {
                mMap.loadMyLocation(makeBounds = false)
                saveMyLocationEnabled(true)
            }
        }
    }
}