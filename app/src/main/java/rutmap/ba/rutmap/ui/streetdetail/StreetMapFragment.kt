package rutmap.ba.rutmap.ui.streetdetail

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.fragment_map.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseMapFragment
import rutmap.ba.rutmap.util.common.getStreetExtra
import rutmap.ba.rutmap.util.data.isMyLocationEnabled
import rutmap.ba.rutmap.util.data.model.loadStreetAsync
import rutmap.ba.rutmap.util.map.calculateBounds
import rutmap.ba.rutmap.util.map.moveToBounds
import rutmap.ba.rutmap.util.map.offline.OMTMapView
import rutmap.ba.rutmap.util.map.offline.Style
import rutmap.ba.rutmap.util.map.showMarkersFamous
import rutmap.ba.rutmap.util.map.showMarkersStreet

class StreetMapFragment : BaseMapFragment() {

    lateinit override var mMapView: OMTMapView
    lateinit override var mMap: MapboxMap
    lateinit var mLocationHint: ImageView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_map,
                container,
                false)
        mLocationHint = view.locationHint

        mMapView = view.mapView

        mMapView.onCreate(savedInstanceState)
        mMapView.getMapAsync {
            mMapView.style = Style.createFromAsset(context, "bright.json")
            mMapView.reapplyStyle()

            mMap = it
            mMap.uiSettings.isLogoEnabled = false
            mMap.uiSettings.setAttributionMargins(0, 0, 0, 0)
            mMap.showMarkersFamous(context, callback = {
                context.loadStreetAsync(getStreetExtra().id, {
                    val street = it

                    mMap.showMarkersStreet(context, street, {
                        if (it.size > 0) {
                            mMap.moveToBounds(calculateBounds(it), context)
                            isMyLocationEnabled()
                        }
                    })
                })
            })
        }
        view.setMyLocationClickListener()

        return view
    }

    override fun showLocationPopup() {
        mLocationHint.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed({
            mLocationHint.visibility = View.GONE
        }, 10000)
    }
}