package rutmap.ba.rutmap.ui.bookmarks

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_bookmarks.*
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Bookmark
import rutmap.ba.rutmap.ui.base.BaseActivity
import rutmap.ba.rutmap.util.common.async
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.data.model.*
import rutmap.ba.rutmap.util.navigateToDetails

class BookmarksActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bookmarks)

        loadBookmarks {
            val bookmarkList = it

            showPlaces(bookmarkList.filter { it.type == "place" }.toList())
            showEvents(bookmarkList.filter { it.type == "event" }.toList())
            showStreets(bookmarkList.filter { it.type == "street" }.toList())
            showGuides(bookmarkList.filter { it.type == "guide" }.toList())

            if (bookmarkList.isEmpty()) {
                textEmpty.visibility = VISIBLE
            }
        }
    }

    private fun showPlaces(bookmarkList: List<Bookmark>) {
        if (bookmarkList.isNotEmpty()) {
            val wrapperPlaces = find<View>(R.id.wrapperPlaces)
            wrapperPlaces.visibility = VISIBLE

            showBookmarks(bookmarkList, containerPlaces)
        }
    }

    private fun showEvents(bookmarkList: List<Bookmark>) {
        if (bookmarkList.isNotEmpty()) {
            val wrapperEvents = find<View>(R.id.wrapperEvents)
            wrapperEvents.visibility = VISIBLE

            showBookmarks(bookmarkList, containerEvents)
        }
    }

    private fun showStreets(bookmarkList: List<Bookmark>) {
        if (bookmarkList.isNotEmpty()) {
            val wrapperStreets = find<View>(R.id.wrapperStreets)
            wrapperStreets.visibility = VISIBLE

            showBookmarks(bookmarkList, containerStreets)
        }
    }

    private fun showGuides(bookmarkList: List<Bookmark>) {
        if (bookmarkList.isNotEmpty()) {
            val wrapperGuides = find<View>(R.id.wrapperGuides)
            wrapperGuides.visibility = VISIBLE

            showBookmarks(bookmarkList, containerGuides)
        }
    }

    private fun showBookmarks(bookmarkList: List<Bookmark>,
                              container: LinearLayout) {
        bookmarkList.forEach {
            val bookmark = it
            container.addView(
                    container.createView(bookmark))
        }
    }

    private fun loadBookmarks(callback: (List<Bookmark>) -> Unit) {
        DataManager(this)
                .loadBookmarks()
                .async()
                .subscribe { callback.invoke(it) }
    }
}

private fun LinearLayout.createView(bookmark: Bookmark): View {
    val inflater = LayoutInflater.from(context)
    val view: View
    if (bookmark.type == "street") {
        view = inflater.inflate(R.layout.item_bookmark_location, this, false)
    } else {
        view = inflater.inflate(R.layout.item_bookmark_category, this, false)
    }
    view.showBookmarkInfo(bookmark)
    view.onClick {
        when (bookmark.type) {
            "place" -> {
                context.loadPlaceAsync(bookmark.id, {
                    val place = it
                    place.navigateToDetails(context)
                })
            }
            "event" -> {
                context.loadEventAsync(bookmark.id, {
                    val event = it
                    event.navigateToDetails(context)
                })
            }
            "street" -> {
                context.loadStreetAsync(bookmark.id, {
                    val guide = it
                    guide.navigateToDetails(context)
                })
            }
            "guide" -> {
                loadGuideAsync(bookmark.id, {
                    val guide = it
                    guide.navigateToDetails(context)
                })
            }
        }
    }
    return view
}

private fun View.showBookmarkInfo(bookmark: Bookmark) {
    val textName = find<TextView>(R.id.textName)
    val textCategory = find<TextView>(R.id.textCategory)

    when (bookmark.type) {
        "place" -> {
            context.loadPlaceAsync(bookmark.id, {
                val place = it
                textName.text = context.getLocalized(place, "name")
                textCategory.showPlaceCategory(place)
            })
        }
        "event" -> {
            context.loadEventAsync(bookmark.id, {
                val event = it
                if (event.id != 0) {
                    textName.text = context.getLocalized(event, "name")
                    textCategory.showEventCategory(event)
                } else {
                    // Event removed, bookmark is obsolete
                    textName.text = "Removed event"
                    setOnClickListener(null)
                    DataManager(context).deleteBookmark(bookmark)
                }
            })
        }
        "street" -> {
            context.loadStreetAsync(bookmark.id, {
                val street = it
                textName.text = context.getLocalized(street, "name")
            })
        }
        "guide" -> {
            loadGuideAsync(bookmark.id, {
                val guide = it
                textName.text = guide.name
                textCategory.visibility = GONE
            })
        }
    }
}