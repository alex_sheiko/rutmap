package rutmap.ba.rutmap.ui.intro

import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_info.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.util.common.english

class InfoActivity : BaseActivityWithSearch() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        Glide.with(this)
                .load(getLocalizedInfo())
                .into(imageView)
    }

    private fun getLocalizedInfo(): Int {
        if (english()) {
            return R.drawable.info_english
        } else {
            return R.drawable.info_bosnian
        }
    }
}
