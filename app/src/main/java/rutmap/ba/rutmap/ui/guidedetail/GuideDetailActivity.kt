package rutmap.ba.rutmap.ui.guidedetail

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.include_tabs_guide.*
import kotlinx.android.synthetic.main.include_toolbar_guide_detail.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivity
import rutmap.ba.rutmap.util.common.getGuideExtra

class GuideDetailActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide_detail)

        setupActionBar()

        setupPages()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val guide = getGuideExtra()
        this.textTitle.text = guide.name
    }

    private fun setupPages() {
        var fragment: Fragment = GuideDetailFragment()

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    1 -> fragment = GuideMapFragment()
                    else -> fragment = GuideDetailFragment()
                }
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit()
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }
        })
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }
}