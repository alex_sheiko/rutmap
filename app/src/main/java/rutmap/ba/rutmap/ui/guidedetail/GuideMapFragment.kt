package rutmap.ba.rutmap.ui.guidedetail

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.fragment_map.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseMapFragment
import rutmap.ba.rutmap.util.common.getGuideExtra
import rutmap.ba.rutmap.util.data.isMyLocationEnabled
import rutmap.ba.rutmap.util.map.calculateBounds
import rutmap.ba.rutmap.util.map.moveToBounds
import rutmap.ba.rutmap.util.map.offline.OMTMapView
import rutmap.ba.rutmap.util.map.offline.Style
import rutmap.ba.rutmap.util.map.showMarkersFamous
import rutmap.ba.rutmap.util.map.showMarkersGuide

class GuideMapFragment : BaseMapFragment() {

    lateinit override var mMapView: OMTMapView
    lateinit override var mMap: MapboxMap
    lateinit var mLocationHint: ImageView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_map,
                container,
                false)
        mLocationHint = view.locationHint

        mMapView = view.mapView
        mMapView.style = Style.createFromAsset(context, "bright.json")

        mMapView.onCreate(savedInstanceState)
        mMapView.getMapAsync {
            mMap = it
            mMap.uiSettings.isLogoEnabled = false
            mMap.uiSettings.setAttributionMargins(0, 0, 0, 0)
            mMap.showMarkersFamous(context, callback = {
                mMap.showMarkersGuide(context, getGuideExtra(), {
                    if (it.size > 0) {
                        val locationList = it
                        if (isMyLocationEnabled()) {
                            mMap.loadMyLocation(locationList, true)
                        } else {
                            mMap.moveToBounds(calculateBounds(locationList), context)
                        }
                    }
                })
            })
        }
        view.setMyLocationClickListener()

        return view
    }

    override fun onStart() {
        super.onStart()
        mMapView.reapplyStyle()
    }

    override fun showLocationPopup() {
        mLocationHint.visibility = VISIBLE
        val handler = Handler()
        handler.postDelayed({
            mLocationHint.visibility = GONE
        }, 10000)
    }
}