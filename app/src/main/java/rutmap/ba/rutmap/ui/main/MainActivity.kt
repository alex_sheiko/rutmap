package rutmap.ba.rutmap.ui.main

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat.START
import android.view.View.OnClickListener
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.include_categories_other.*
import kotlinx.android.synthetic.main.include_category_events.*
import kotlinx.android.synthetic.main.include_category_guide.*
import kotlinx.android.synthetic.main.include_category_streets.*
import kotlinx.android.synthetic.main.include_toolbar_main.*
import kotlinx.android.synthetic.main.nav_main.*
import org.jetbrains.anko.*
import rutmap.ba.rutmap.BuildConfig.DEBUG
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.data.remote.MyAlarmReceiver
import rutmap.ba.rutmap.data.remote.SyncService
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.ui.bookmarks.BookmarksActivity
import rutmap.ba.rutmap.ui.eventlist.EventListActivity
import rutmap.ba.rutmap.ui.guidelist.GuideListActivity
import rutmap.ba.rutmap.ui.intro.IntroActivity
import rutmap.ba.rutmap.ui.placelist.PlaceListActivity
import rutmap.ba.rutmap.ui.streetlist.StreetListActivity
import rutmap.ba.rutmap.util.common.async
import rutmap.ba.rutmap.util.common.restart
import rutmap.ba.rutmap.util.common.setLocale
import rutmap.ba.rutmap.util.data.firstLaunch
import rutmap.ba.rutmap.util.data.getLocale
import rutmap.ba.rutmap.util.data.loadLocalCategory
import rutmap.ba.rutmap.util.design.getChildren
import rutmap.ba.rutmap.util.design.image.showImage
import rutmap.ba.rutmap.util.design.image.showImageFit
import rutmap.ba.rutmap.util.design.setupSlidingMenu

class MainActivity : BaseActivityWithSearch() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (firstLaunch()) {
            navigateToChooseLanguage()
            return
        }
        setLocale(getLocale())
        setContentView(R.layout.activity_main)

        setupActionBar()
        setupSlidingMenu()
        setClickListeners()

        showData()

        fixEmptyDatabases()
        scheduleSync()
    }

    private fun scheduleSync() {
        val startTime: Long = if (DEBUG) {
            60 * 1000L // 1 min
        } else {
            20 * 60 * 1000L // 20 min
        }
        val syncInterval: Long = if (DEBUG) {
            5 * 60 * 1000L // 5 min
        } else {
            6 * 60 * 60 * 1000L // 6 hours
        }
        val intent = Intent(applicationContext, MyAlarmReceiver::class.java)
        val pIntent = PendingIntent.getBroadcast(this, MyAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, startTime,
                syncInterval, pIntent)
    }

    private fun fixEmptyDatabases() {
        DataManager(this@MainActivity)
                .loadStreets()
                .async()
                .subscribe({
                    if (it.isEmpty()) {
                        startService<SyncService>()
                    }
                })
    }

    private fun navigateToChooseLanguage() {
        startActivity<IntroActivity>()
        finishAffinity()
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(START)) {
            drawer.closeDrawer(START)
        } else {
            super.onBackPressed()
        }
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setClickListeners() {
        containerEvents.onClick {
            startActivity<EventListActivity>()
        }
        containerGuides.onClick {
            startActivity<GuideListActivity>()
        }
        containerStreets.onClick {
            startActivity<StreetListActivity>()
        }
        buttonCategories.onClick {
            startActivity<PlaceListActivity>()
        }
        val listener = OnClickListener {
            showCategory(it.tag)
        }
        containerPrimaryCategories.getChildren()
                .filter { it is Button }
                .forEach { it.setOnClickListener(listener) }
        buttonBookmarksOpen.onClick {
            startActivity<BookmarksActivity>()
        }
        buttonEnglish.onClick { restart("en") }
        buttonBosnian.onClick { restart("bs") }
    }

    private fun showData() {
        imageEvents.showImage(this, R.drawable.bg_category_events)
        imageGuide.showImage(this, R.drawable.bg_category_guide)
        imageStreets.showImage(this, R.drawable.bg_category_streets)
        imageAd.showImageFit(this, R.drawable.ic_ad_banner)
    }
}

fun Context.navigateToPlaces(category: Category?) {
    if (category != null) {
        startActivity<PlaceListActivity>(
                "category" to category)
    } else {
        startActivity<PlaceListActivity>(
                "ad" to true)
    }
}

fun Activity.showCategory(tag: Any) {
    val categoryId = Integer.parseInt(tag.toString())
    doAsync {
        val category = DataManager(this@showCategory).loadLocalCategory(categoryId)
        uiThread {
            navigateToPlaces(category)
        }
    }
}
