package rutmap.ba.rutmap.ui.eventdetail

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_location_detail.view.*
import kotlinx.android.synthetic.main.include_event_info.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.util.common.eventMode
import rutmap.ba.rutmap.util.common.getEventExtra
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.common.getPlaceExtra
import rutmap.ba.rutmap.util.data.formatTime
import rutmap.ba.rutmap.util.data.model.*
import rutmap.ba.rutmap.util.data.setBookmarkListener

class LocationDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_location_detail,
                container,
                false)

        if (eventMode()) {
            val event = getEventExtra()
            showData(view, event)
            view.setBookmarkListener(event.id, "event")
        } else {
            val place = getPlaceExtra()!!
            showData(view, place)
            view.setBookmarkListener(place.id, "place")
        }
        return view
    }

    private fun showData(v: View, item: Any) {
        showHeader(v, item)
        showDetails(v, item)
    }

    @SuppressLint("SetTextI18n")
    private fun showHeader(v: View, item: Any) {
        val ctx = activity

        if (item is Event) {
            val event = item
            v.textTitle.text = ctx.getLocalized(event, "name")
            v.textDate.text = formatTime(event.eventTime)
            v.textPlace.showPlace(ctx, event)
            v.textCategory.showEventCategory(event)
            v.imageView.showEventImage(ctx, event)
        } else if (item is Place) {
            val place = item
            v.textTitle.text = ctx.getLocalized(place, "name")
            v.textCategory.showPlaceCategory(place)
            v.textHeaderDetails.text = getString(R.string.header_description)

            v.textDate.text = place.workTime
                    ?.replace(" | ", "\n") // break line for multi-day schedules
            place.loadSliderAsync(context, {
                val slider = it
                v.imageView.showPlaceImage(ctx, slider)
            })
            v.textPlace.showStreet(ctx, place)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDetails(v: View, item: Any) {
        if (item is Event) {
            val event = item
            v.textPhone.text = event.tel
            v.textEmail.text = event.email
            v.textDesc.text = formatDesc(
                    activity.getLocalized(event, "details"),
                    activity.getLocalized(event, "name"))

            if (event.email == null || event.email.isEmpty()) {
                v.textEmail.visibility = GONE
            }
        } else if (item is Place) {
            val place = item
            v.textDesc.text = place.formattedDescription
            v.textTags.visibility = VISIBLE
            v.textTags.text = getString(R.string.tags) + " " + context.getLocalized(place, "tags")

            v.textPhone.text = "+387${place.telefon.replace(" ", "")}".replace("+387+387", "+387")
            v.textEmail.text = place.mail
            v.textWeb.text = place.web

            if (place.mail.isEmpty() || place.mail == "NULA") {
                v.textEmail.visibility = GONE
            } else {
                v.textEmail.visibility = VISIBLE
            }
            if (place.web == null || place.web.isEmpty() || place.web == "NULA") {
                v.textWeb.visibility = GONE
            } else {
                v.textWeb.visibility = VISIBLE
            }
            if (place.description == "NULA") {
                v.textDesc.visibility = GONE
                v.textHeaderDetails.visibility = GONE
            }
        }
    }
}