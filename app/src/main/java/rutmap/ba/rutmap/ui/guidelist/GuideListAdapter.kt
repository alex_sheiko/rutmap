package rutmap.ba.rutmap.ui.guidelist

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.include_guide_audio_label.view.*
import kotlinx.android.synthetic.main.include_guide_header_text.view.*
import kotlinx.android.synthetic.main.item_guide.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Guide
import rutmap.ba.rutmap.ui.base.BaseAdapter
import rutmap.ba.rutmap.ui.guidedetail.GuideDetailActivity
import rutmap.ba.rutmap.ui.guidelist.GuideListAdapter.ViewHolder
import rutmap.ba.rutmap.util.design.image.showImage

class GuideListAdapter(val ctx: Context)
    : BaseAdapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = from(parent.context)
                .inflate(R.layout.item_guide, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val guide = dataset[position] as Guide

        holder.showData(guide)

        holder.setClickListener(guide)
    }

    private fun ViewHolder.setClickListener(guide: Guide) {
        itemView.onClick {
            val activity = ctx as Activity

            activity.startActivity<GuideDetailActivity>(
                    "guide" to guide
            )
        }
    }

    private fun ViewHolder.showData(guide: Guide) {
        textName.text = guide.name
        textDistance.text = guide.distance
        textDuration.text = guide.duration
        imageGuide.showImage(ctx, guide.imageId)

        if (guide.hasAudio) {
            labelAudio.visibility = VISIBLE
        } else {
            labelAudio.visibility = GONE
        }
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val textName: TextView = v.textName
        val textDuration: TextView = v.textDuration
        val textDistance: TextView = v.textDistance
        val imageGuide: ImageView = v.imageGuide
        val labelAudio: TextView = v.labelAudio
    }
}