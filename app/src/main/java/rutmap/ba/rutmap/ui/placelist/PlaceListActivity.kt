package rutmap.ba.rutmap.ui.placelist

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.include_tabs_place.*
import kotlinx.android.synthetic.main.include_toolbar_place_list.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.ui.placelist.map.PlaceListMapFragment
import rutmap.ba.rutmap.util.common.getCategoryExtra
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.design.image.getIcon
import rutmap.ba.rutmap.util.design.image.showImageFit

class PlaceListActivity : BaseActivityWithSearch() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_list)

        setupActionBar()
        setupPages()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val category = getCategoryExtra()
        if (category != null) {
            textTitle.text = getLocalized(category, "name")
            iconTitle.showImageFit(this,
                    category.getIcon())
        } else if (intent.hasExtra("ad")) {
            textTitle.text = "!hej"
            iconTitle.showImageFit(this,
                    R.drawable.ic_ad_logo)
        } else {
            textTitle.text = getString(R.string.title_all_categories)
            iconTitle.showImageFit(this,
                    R.drawable.ic_all_categories)
        }
    }

    private fun setupPages() {
        var fragment: Fragment = PlaceListMapFragment(false)

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.position) {
                    2 -> fragment = PlaceListFragment()
                    1 -> fragment = PlaceListMapFragment(true, true)
                    else -> fragment = PlaceListMapFragment(false)
                }
                supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .commit()
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
            }
        })
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commit()
    }
}