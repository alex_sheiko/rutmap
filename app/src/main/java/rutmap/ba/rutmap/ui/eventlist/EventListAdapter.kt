package rutmap.ba.rutmap.ui.eventlist

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.ui.base.BaseAdapter
import rutmap.ba.rutmap.ui.eventdetail.EventDetailActivity
import rutmap.ba.rutmap.ui.eventlist.EventListAdapter.ViewHolder
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.data.formatTime
import rutmap.ba.rutmap.util.data.model.showEventCategory
import rutmap.ba.rutmap.util.data.model.showEventImage
import rutmap.ba.rutmap.util.data.model.showPlace

class EventListAdapter(val ctx: Context)
    : BaseAdapter<ViewHolder>() {

    companion object {
        val TYPE_SPECIAL = 1
        val TYPE_REGULAR = 0
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = from(parent.context)
                .inflate(R.layout.item_event, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val event = dataset[position] as Event

        showData(holder, event)

        setClickListener(holder, event)
    }

    override fun getItemViewType(position: Int): Int {
        val event = dataset[position] as Event
        if (event.spec == 1) {
            return TYPE_SPECIAL
        } else {
            return TYPE_REGULAR
        }
    }

    private fun setClickListener(holder: ViewHolder, event: Event) {
        holder.itemView.onClick {
            val activity = ctx as Activity

            activity.startActivity<EventDetailActivity>(
                    "event" to event
            )
        }
    }

    private fun showData(holder: ViewHolder, event: Event) {
        holder.textTitle.text = ctx.getLocalized(event, "name")
        holder.textDate.text = formatTime(event.eventTime)

        holder.textCategory.showEventCategory(event)
        holder.textPlace.showPlace(ctx, event)
        holder.image.showEventImage(ctx, event)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val textCategory: TextView = v.find<TextView>(R.id.textCategory)
        val textTitle: TextView = v.find<TextView>(R.id.textTitle)
        val textPlace: TextView = v.find<TextView>(R.id.textPlace)
        val textDate: TextView = v.find<TextView>(R.id.textDate)
        val image: ImageView = v.find<ImageView>(R.id.image)
    }
}