package rutmap.ba.rutmap.ui.intro

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_intro.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.local.database
import rutmap.ba.rutmap.util.common.goHome
import rutmap.ba.rutmap.util.data.createTables
import rutmap.ba.rutmap.util.data.dropTables
import rutmap.ba.rutmap.util.data.insertData
import rutmap.ba.rutmap.util.design.image.showImage

class IntroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        showImages()

        setClickListeners()

        database.use {
            dropTables()
            createTables()

            insertData(ctx)
        }
    }

    private fun setClickListeners() {
        buttonEnglish.onClick { goHome("en") }
        buttonBosnian.onClick { goHome("bs") }
    }

    private fun showImages() {
        navBackground.showImage(this, R.drawable.nav_bg)
    }
}
