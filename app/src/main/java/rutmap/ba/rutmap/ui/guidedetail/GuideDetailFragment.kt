package rutmap.ba.rutmap.ui.guidedetail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.include_guide_audio_download.view.*
import kotlinx.android.synthetic.main.include_guide_book.view.*
import kotlinx.android.synthetic.main.include_guide_detail_header.view.*
import kotlinx.android.synthetic.main.include_guide_details_body.view.*
import kotlinx.android.synthetic.main.include_guide_header_text.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.util.common.getGuideExtra
import rutmap.ba.rutmap.util.data.downloadAll
import rutmap.ba.rutmap.util.data.model.getGuideId
import rutmap.ba.rutmap.util.data.model.showGuidePlaces
import rutmap.ba.rutmap.util.data.model.stopAudio
import rutmap.ba.rutmap.util.data.setBookmarkListener
import rutmap.ba.rutmap.util.design.image.showImage

class GuideDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_guide_detail,
                container,
                false)

        view.setClickListeners()
        view.showData()

        return view
    }

    private fun View.showData() {
        val guide = getGuideExtra()

        this.textName.text = guide.name
        this.textDesc.text = guide.desc
        this.textDistance.text = guide.distance
        this.textDuration.text = guide.duration
        this.imageGuide.showImage(activity, guide.imageId)
        this.containerPlaces.showGuidePlaces(guide.placeArray, guide.distanceArray, buttonAudio)
    }

    private fun View.setClickListeners() {
        val guide = getGuideExtra()

        buttonAudio.onClick {
            buttonAudio.text = getString(R.string.downloading)
            activity.downloadAll({
                buttonAudio.text = getString(R.string.downloaded)
            })
        }
        buttonBook.onClick {
            activity.startActivity<WebviewActivity>(
                    "url" to guide.bookUrl)
        }
        setBookmarkListener(guide.getGuideId(), "guide")
    }

    override fun onDestroy() {
        super.onDestroy()
        stopAudio()
    }
}