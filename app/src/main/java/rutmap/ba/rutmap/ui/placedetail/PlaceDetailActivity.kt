package rutmap.ba.rutmap.ui.placedetail

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.include_tabs_detail.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.ui.eventdetail.LocationDetailFragment
import rutmap.ba.rutmap.ui.eventdetail.streetview.StreetViewFragment
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.common.getPlaceExtra
import rutmap.ba.rutmap.util.data.model.loadPlaceAsync

class PlaceDetailActivity : BaseActivityWithSearch() {

    lateinit var place: Place

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_detail)

        if (intent.hasExtra("place")) {
            place = getPlaceExtra()!!

            setupActionBar()

            setupPages()
        } else {
            loadPlaceAsync(intent.getStringExtra("placeName"), {
                place = it

                setupActionBar()

                setupPages()
            })
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out)
    }

    private fun setupActionBar() {
        title = getLocalized(place, "name")
    }

    private fun setupPages() {
        pager.adapter = SectionsPagerAdapter(supportFragmentManager)

        pager.addOnPageChangeListener(
                TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(
                TabLayout.ViewPagerOnTabSelectedListener(pager))
    }

    inner class SectionsPagerAdapter
    constructor(fm: FragmentManager) :
            FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                1 -> return PlaceMapFragment()
                2 -> return StreetViewFragment()
                else -> return LocationDetailFragment()
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }
}
