package rutmap.ba.rutmap.ui.placelist

import android.app.Activity
import android.content.Context
import android.location.Location
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.ui.base.BaseAdapter
import rutmap.ba.rutmap.ui.placedetail.PlaceDetailActivity
import rutmap.ba.rutmap.ui.placelist.PlaceListAdapter.ViewHolder
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.data.model.getLocation
import rutmap.ba.rutmap.util.data.model.showName
import rutmap.ba.rutmap.util.data.model.showPlaceCategory
import rutmap.ba.rutmap.util.data.model.showStreet
import rutmap.ba.rutmap.util.map.getDistanceTo

class PlaceListAdapter(val ctx: Context)
    : BaseAdapter<ViewHolder>() {

    var myLocation: Location? = null

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val v = from(parent.context)
                .inflate(R.layout.item_place, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val place = dataset[position] as Place

        holder.showData(ctx, place)

        holder.setClickListener(place)
    }

    private fun ViewHolder.setClickListener(place: Place) {
        itemView.onClick {
            val activity = ctx as Activity

            activity.startActivity<PlaceDetailActivity>(
                    "place" to place
            )
        }
    }

    fun ViewHolder.showData(ctx: Context, place: Place) {
        textName.showName(place)
        textDistance.text = myLocation.getDistanceTo(place.getLocation())
        textTags.text = ctx.getLocalized(place, "name")
        textStreet.showStreet(ctx, place)
        textCategory.showPlaceCategory(place)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val textName: TextView = v.find<TextView>(R.id.textName)
        val textStreet: TextView = v.find<TextView>(R.id.textStreet)
        val textDistance: TextView = v.find<TextView>(R.id.textDistance)
        val textCategory: TextView = v.find<TextView>(R.id.textCategory)
        val textTags: TextView = v.find<TextView>(R.id.textTags)
    }
}