package rutmap.ba.rutmap.ui.main

import rutmap.ba.rutmap.data.DataManager

class MainPresenter(val view: MainActivity) {

    val mDataManager = DataManager(view)
}