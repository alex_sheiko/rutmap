package rutmap.ba.rutmap.ui.eventlist

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.RadioButton
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_event_list.*
import kotlinx.android.synthetic.main.include_footer_event_list.*
import kotlinx.android.synthetic.main.include_toolbar_event_list.*
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.ui.eventlist.EventListAdapter.Companion.TYPE_SPECIAL
import rutmap.ba.rutmap.util.data.convertToDate
import rutmap.ba.rutmap.util.data.isSameDay
import rutmap.ba.rutmap.util.design.setupDateSelector
import java.lang.Integer.parseInt
import java.util.*
import java.util.Collections.sort

class EventListActivity : BaseActivityWithSearch() {

    val mAdapter by lazy { EventListAdapter(this) }
    private val mPresenter by lazy { EventListPresenter(this) }

    lateinit var allEvents: List<Event>
    private lateinit var dateFilteredEvents: List<Event>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_list)

        setupActionBar()
        setupRecyclerView()
        setupDateSelector()

        setClickListeners()

        showData()
    }

    fun populateList(events: List<Event>) {
        val previousSize = mAdapter.itemCount

        mAdapter.clear()
        mAdapter.notifyItemRangeRemoved(0, previousSize)
        mAdapter.addAll(events)
        mAdapter.notifyItemRangeInserted(0, events.size)
    }

    fun showForDate(selectedDate: Date) {
        dateFilteredEvents = allEvents.filter {
            val eventDate = it.eventTime.convertToDate()
            eventDate.isSameDay(selectedDate)
        }
        sort(dateFilteredEvents, { e1, e2 -> e2.spec - e1.spec })
        populateList(dateFilteredEvents)
    }

    private fun setClickListeners() {
        categoryGroup.setOnCheckedChangeListener { _, checkedId ->
            if (findViewById<View>(checkedId) != null) {
                val checkedButton = find<RadioButton>(checkedId)
                val query = parseInt(checkedButton.tag.toString())
                val filteredEvents = dateFilteredEvents.filter {
                    it.catId == query
                }
                sort(filteredEvents, { e1, e2 -> e2.spec - e1.spec })
                populateList(filteredEvents)

                if (filteredEvents.isEmpty()) {
                    toast("No events for this filter")
                }
            } else {
                populateList(allEvents)
            }
        }
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.elevation = 0f
    }

    private fun setupRecyclerView() {
        val layoutManager = GridLayoutManager(this, 2)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (mAdapter.getItemViewType(position) == TYPE_SPECIAL) 2 else 1
            }
        }
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = mAdapter
        recyclerView.itemAnimator = SlideInUpAnimator(
                OvershootInterpolator(1f))
    }

    private fun showData() {
        mPresenter.showEvents()
    }

    fun showLoading(show: Boolean) {
        val refreshLayout = find<SwipeRefreshLayout>(R.id.refreshLayout)
        refreshLayout.isRefreshing = show
    }
}