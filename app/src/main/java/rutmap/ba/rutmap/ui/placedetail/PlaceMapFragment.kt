package rutmap.ba.rutmap.ui.placedetail

import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newLatLngZoom
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import kotlinx.android.synthetic.main.fragment_map.view.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.ui.base.BaseMapFragment
import rutmap.ba.rutmap.util.common.getEventExtra
import rutmap.ba.rutmap.util.common.getPlaceExtra
import rutmap.ba.rutmap.util.data.isMyLocationEnabled
import rutmap.ba.rutmap.util.data.model.getLocation
import rutmap.ba.rutmap.util.data.model.loadPlaceAsync
import rutmap.ba.rutmap.util.data.saveLocation
import rutmap.ba.rutmap.util.map.calculateBounds
import rutmap.ba.rutmap.util.map.moveToBounds
import rutmap.ba.rutmap.util.map.offline.OMTMapView
import rutmap.ba.rutmap.util.map.offline.Style
import rutmap.ba.rutmap.util.map.showMarkerDefault
import rutmap.ba.rutmap.util.map.showMarkersFamous

class PlaceMapFragment : BaseMapFragment() {

    lateinit override var mMapView: OMTMapView
    lateinit override var mMap: MapboxMap
    lateinit var mLocationHint: ImageView
    lateinit var mPlaceLocation: Location

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_map,
                container,
                false)
        mLocationHint = view.locationHint

        mMapView = view.mapView
        mMapView.style = Style.createFromAsset(context, "bright.json")

        mMapView.onCreate(savedInstanceState)
        mMapView.getMapAsync {
            mMap = it
            mMap.uiSettings.isLogoEnabled = false
            mMap.uiSettings.setAttributionMargins(0, 0, 0, 0)
            mMap.showMarkersFamous(context, callback = {

                if (getPlaceExtra() != null) {
                    showPlace(getPlaceExtra()!!)
                    Handler().postDelayed({
                        if (isMyLocationEnabled()) {
                            mMap.loadMyLocation(makeBounds = false)
                        } else {
                            mMap.animateCamera(
                                    newLatLngZoom(LatLng(mPlaceLocation),
                                            16.0))
                        }
                    }, 1000)
                } else {
                    getEventExtra().loadPlaceAsync(activity, {
                        showPlace(it)
                        if (isMyLocationEnabled()) {
                            mMap.loadMyLocation(makeBounds = false)
                        } else {
                            mMap.animateCamera(
                                    newLatLngZoom(LatLng(mPlaceLocation),
                                            16.0))
                        }
                    })
                }
            })
        }
        view.setMyLocationClickListener()

        return view
    }

    override fun onStart() {
        super.onStart()
        mMapView.reapplyStyle()
    }

    private fun showPlace(it: Place) {
        mPlaceLocation = it.getLocation()

        mMap.showMarkerDefault(mPlaceLocation)
    }

    override fun MapboxMap.forceLocationUpdate(
            locationList: ArrayList<Location>?,
            makeBounds: Boolean) {
        isMyLocationEnabled = true

        if (myLocation != null) {
            moveToBounds(calculateBounds(listOf(
                    mPlaceLocation,
                    myLocation!!)), context)
            context?.saveLocation(myLocation!!)
        } else {
            setOnMyLocationChangeListener { myLocation ->
                if (myLocation != null) {
                    moveToBounds(calculateBounds(listOf(
                            mPlaceLocation,
                            myLocation)), context)
                    context?.saveLocation(myLocation)
                }
                setOnMyLocationChangeListener(null)
            }
        }
    }

    override fun showLocationPopup() {
        mLocationHint.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed({
            mLocationHint.visibility = View.GONE
        }, 10000)
    }
}