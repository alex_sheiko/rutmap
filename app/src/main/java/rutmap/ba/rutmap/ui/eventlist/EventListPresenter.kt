package rutmap.ba.rutmap.ui.eventlist

import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.transaction
import org.jetbrains.anko.doAsync
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.local.database
import rutmap.ba.rutmap.data.local.table.EventsTable
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.util.common.async
import rutmap.ba.rutmap.util.data.today

class EventListPresenter(val view: EventListActivity) {

    private var mNeedsRemoteEvents: Boolean = false
    val mDataManager = DataManager(view)

    fun showEvents() {
        view.showLoading(true)

        showLocalEvents()
        downloadRemoteEvents()
    }

    private fun showLocalEvents() {
        mDataManager.loadLocalEventsObs()
                .async()
                .subscribe({
                    showEvents(it)
                })
    }

    private fun showEvents(it: List<Event>) {
        if (it.isEmpty()) {
            mNeedsRemoteEvents = true
            return
        }
        val showForToday = view.mAdapter.itemCount == 0

        view.allEvents = it
        view.populateList(it)
        view.showLoading(false)

        if (showForToday) {
            view.showForDate(today())
        }
    }

    private fun downloadRemoteEvents() {
        mDataManager.loadRemoteEventsObs()
                .async()
                .subscribe({
                    if (it.isNotEmpty()) {
                        if (mNeedsRemoteEvents) {
                            showEvents(it)
                            mNeedsRemoteEvents = false
                        }
                        doAsync {
                            view.database.use {
                                val eventList = it
                                transaction {
                                    eventList.forEach {
                                        try {
                                            val event = it
                                            insert(EventsTable.NAME,
                                                    "id" to event.id,
                                                    "title" to event.title,
                                                    "titleEn" to event.titleEn,
                                                    "objectId" to event.objectId,
                                                    "alias" to event.alias,
                                                    "aliasEn" to event.aliasEn,
                                                    "details" to event.details,
                                                    "detailsEn" to event.detailsEn,
                                                    "mainPic" to event.mainPic,
                                                    "bigimg" to event.bigimg,
                                                    "tel" to event.tel,
                                                    "eventTime" to event.eventTime,
                                                    "catId" to event.catId,
                                                    "email" to event.email,
                                                    "published" to event.published,
                                                    "sff" to event.sff,
                                                    "spec" to event.spec)
                                        } catch (_: Exception) {
                                            // Event already inserted
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
    }
}