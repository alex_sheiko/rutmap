package rutmap.ba.rutmap.ui.streetdetail

import android.os.Bundle
import kotlinx.android.synthetic.main.include_toolbar_street_map.*
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.base.BaseActivityWithSearch
import rutmap.ba.rutmap.util.common.getStreetExtra

class StreetMapActivity : BaseActivityWithSearch() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_street_map)

        setupActionBar()

        showMap()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        textTitle.text = getStreetExtra().name
    }

    private fun showMap() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, StreetMapFragment())
                .commit()
    }
}
