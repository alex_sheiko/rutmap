package rutmap.ba.rutmap.ui.placelist.header

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_place_premium.view.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Slider
import rutmap.ba.rutmap.ui.placedetail.PlaceDetailActivity
import rutmap.ba.rutmap.util.data.model.formattedDescription
import rutmap.ba.rutmap.util.data.model.loadPlaceAsync
import rutmap.ba.rutmap.util.data.model.loadSliderAsync
import rutmap.ba.rutmap.util.data.model.showPlaceImage

class PlacesPremiumFragment(val placeId: Int) : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(
                R.layout.fragment_place_premium,
                container,
                false)

        context.loadPlaceAsync(placeId, {

            val place = it
            place.loadSliderAsync(context, {

                val slider = it
                view.showData(place, slider)
                view.setClickListener(place)
            })
        })

        return view
    }

    private fun View.showData(place: Place, slider: Slider) {
        textName.text = place.naziv
        if (place.description.contains("!hej Tourist tariff")) {
            textDesc.text = "Enjoy your vacation, be online, share photos."
        } else {
            textDesc.text = place.formattedDescription
        }
        imageHeader.showPlaceImage(context, slider)
    }
}

@Suppress("UNUSED_PARAMETER")
private fun View.setClickListener(place: Place) {
    onClick {
        context.startActivity<PlaceDetailActivity>("place" to place)
    }
}
