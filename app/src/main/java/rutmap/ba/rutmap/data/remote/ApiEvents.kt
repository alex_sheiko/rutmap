package rutmap.ba.rutmap.data.remote

import retrofit2.Call
import retrofit2.http.GET

interface ApiEvents {

    @GET("rUtap1.php?action=desavanja")
    fun getEvents(): Call<EventsResponse>
}