package rutmap.ba.rutmap.data.model

data class EventCategory(
        val id: Int,
        val title: String,
        val group: String,
        val titleEn: String
)