package rutmap.ba.rutmap.data.remote

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.google.gson.Gson
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.db.dropTable
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.transaction
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.local.database
import rutmap.ba.rutmap.data.local.table.PlacesTable
import rutmap.ba.rutmap.data.local.table.StreetsTable

class SyncService : IntentService("SyncService") {

    override fun onHandleIntent(intent: Intent?) {
        Log.d("Sync", "Running sync")

        val dataManager = DataManager(this)

        dataManager.loadPlacesObs()
                .subscribeOn(Schedulers.io())
                .subscribe({
                    val placeList = it
                    if (placeList.isNotEmpty()) {
                        database.use {
                            dropTable(PlacesTable.NAME, true)
                            PlacesTable.createWith(this)

                            transaction {
                                placeList.forEach {
                                    val place = it
                                    insert(PlacesTable.NAME,
                                            "id" to place.id,
                                            "naziv" to place.naziv,
                                            "nazivEn" to place.nazivEn,
                                            "alias" to place.alias,
                                            "description" to place.description,
                                            "descriptionEn" to place.descriptionEn,
                                            "category" to place.category,
                                            "categoryParent" to place.categoryParent,
                                            "categoryEn" to place.categoryEn,
                                            "categoryParentEn" to place.categoryParentEn,
                                            "cat_id" to place.cat_id,
                                            "parentCatId" to place.parentCatId,
                                            "coords" to place.coords,
                                            "lat" to place.lat,
                                            "lon" to place.lon,
                                            "centar1" to place.centar1,
                                            "centar2" to place.centar2,
                                            "centar3" to place.centar3,
                                            "centar4" to place.centar4,
                                            "opSlika" to place.opSlika,
                                            "opImgUrl" to place.opImgUrl,
                                            "opImgUrl2" to place.opImgUrl2,
                                            "opImgUrl3" to place.opImgUrl3,
                                            "objectClassImg" to place.objectClassImg,
                                            "telefon" to place.telefon,
                                            "mail" to place.mail,
                                            "web" to place.web,
                                            "slika" to place.slika,
                                            "slider" to place.slider,
                                            "address" to place.address,
                                            "street_id" to place.street_id,
                                            "people" to place.people,
                                            "rating" to place.rating,
                                            "categoryClass" to place.categoryClass,
                                            "vrsta" to place.vrsta,
                                            "streetNumber" to place.streetNumber,
                                            "minZoom" to place.minZoom,
                                            "package" to place.`package`,
                                            "showOnSearch" to place.showOnSearch,
                                            "tags" to place.tags,
                                            "tagsEn" to place.tagsEn,
                                            "workTime" to place.workTime)
                                }
                            }
                        }
                    }
                })
        dataManager.loadStreetsObs()
                .subscribeOn(Schedulers.io())
                .subscribe({
                    val streetList = it
                    if (streetList.isNotEmpty()) {
                        database.use {
                            dropTable(StreetsTable.NAME, true)
                            StreetsTable.createWith(this)

                            transaction {
                                streetList.forEach {
                                    val street = it
                                    insert(StreetsTable.NAME,
                                            "id" to street.id,
                                            "name" to street.name,
                                            "full_name" to street.full_name,
                                            "alias" to street.alias,
                                            "coords" to street.coords,
                                            "polilines" to street.polilines,
                                            "municipality_id" to street.municipality_id,
                                            "points" to Gson().toJson(street.points))
                                }
                            }
                        }
                    }
                })
    }
}