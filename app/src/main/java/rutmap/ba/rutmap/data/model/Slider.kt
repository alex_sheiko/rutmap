package rutmap.ba.rutmap.data.model

data class Slider(
        val id: Int,
        val object_id: Int,
        val image: String,
        val sort: Int)