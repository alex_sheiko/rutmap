package rutmap.ba.rutmap.data.model

data class Bookmark(
        val id: Int,
        val type: String)