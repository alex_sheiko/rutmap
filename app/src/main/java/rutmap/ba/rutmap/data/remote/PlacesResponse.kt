package rutmap.ba.rutmap.data.remote

import rutmap.ba.rutmap.data.model.Place

data class PlacesResponse(val data: List<Place>)