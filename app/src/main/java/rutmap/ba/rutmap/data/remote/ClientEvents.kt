package rutmap.ba.rutmap.data.remote

import android.content.Context
import com.google.gson.Gson
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Event
import java.io.File
import java.util.concurrent.TimeUnit.SECONDS


class ClientEvents(val context: Context) {

    private val mApi: ApiEvents

    init {
        val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(CacheInterceptor(context))
                .cache(Cache(File(context.cacheDir,
                        "apiResponses"), 20 * 1024 * 1024)) // 20 MB
                .connectTimeout(30, SECONDS)
                .readTimeout(60 * 5, SECONDS)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://www.rutmap.ba/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(okHttpClient)
                .build()

        mApi = retrofit.create(ApiEvents::class.java)
    }

    fun loadRemoteEvents(): List<Event> {
        try {
            val response = mApi.getEvents().execute().body()
            return response!!.data
        } catch (e: Exception) {
            return emptyList()
        }
    }
}

fun DataManager.loadRemoteEvents(): List<Event> {
    return ClientEvents(ctx).loadRemoteEvents()
}