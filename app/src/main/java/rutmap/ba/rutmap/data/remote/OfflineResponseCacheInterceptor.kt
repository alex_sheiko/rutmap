package rutmap.ba.rutmap.data.remote

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import rutmap.ba.rutmap.BuildConfig.DEBUG
import java.io.IOException

class CacheInterceptor(val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val syncInterval: Long = if (DEBUG) {
            60 // 1 min
        } else {
            6 * 60 * 60 // 6 hours
        }
        val cachingHeader: String = if (context.isNetworkAvailable()) {
            "public, max-age=$syncInterval"
        } else {
            "public, only-if-cached, max-stale=$syncInterval"
        }
        val response = chain.proceed(chain.request())
        return response.newBuilder()
                .header("Cache-Control", cachingHeader)
                .build()
    }
}

private fun Context.isNetworkAvailable(): Boolean {
    val connectManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connectManager.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnectedOrConnecting
}