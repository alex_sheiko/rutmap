package rutmap.ba.rutmap.data.remote

import android.content.Context
import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Street
import java.io.File
import java.io.StringReader
import java.util.concurrent.TimeUnit.SECONDS


class ClientPlacesStreets(val context: Context) {

    private val mApi: ApiPlacesStreets

    init {
        val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(CacheInterceptor(context))
                .cache(Cache(File(context.cacheDir,
                        "apiResponses"), 20 * 1024 * 1024)) // 20 MB
                .connectTimeout(30, SECONDS)
                .readTimeout(60 * 5, SECONDS)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://54.93.253.40/appsync/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(okHttpClient)
                .build()

        mApi = retrofit.create(ApiPlacesStreets::class.java)
    }

    fun loadRemotePlaces(): List<Place> {
        try {
            val responseBody = mApi.getPlacesResponse().execute().body()
            val json = responseBody?.string()

            val reader = JsonReader(StringReader(json))
            reader.isLenient = true

            val response = Gson().fromJson<PlacesResponse>(
                    reader, PlacesResponse::class.java)
            return response.data
        } catch (e: Exception) {
            e.printStackTrace()
            return emptyList()
        }
    }

    fun loadRemoteStreets(): List<Street> {
        try {
            val responseBody = mApi.getStreetsResponse().execute().body()
            val json = responseBody?.string()

            val reader = JsonReader(StringReader(json))
            reader.isLenient = true

            val response = Gson().fromJson<StreetsResponse>(
                    reader, StreetsResponse::class.java)
            return response.data
        } catch (e: Exception) {
            e.printStackTrace()
            return emptyList()
        }
    }
}

fun DataManager.loadRemotePlaces(): List<Place> {
    return ClientPlacesStreets(ctx).loadRemotePlaces()
}

fun DataManager.loadRemoteStreets(): List<Street> {
    return ClientPlacesStreets(ctx).loadRemoteStreets()
}