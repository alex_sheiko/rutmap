package rutmap.ba.rutmap.data.model

import io.mironov.smuggler.AutoParcelable

data class Guide(
        val name: String,
        val desc: String,
        val bookUrl: String,
        val alias: String,
        val distance: String,
        val duration: String,
        val imageId: Int,
        val placeArray: Array<Int>,
        val distanceArray: Array<String>,
        val hasAudio: Boolean
) : AutoParcelable