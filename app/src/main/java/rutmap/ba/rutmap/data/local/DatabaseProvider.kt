package rutmap.ba.rutmap.data.local

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.ManagedSQLiteOpenHelper
import rutmap.ba.rutmap.util.data.createTables
import rutmap.ba.rutmap.util.data.dropTables

class DatabaseProvider(val ctx: Context) :
        ManagedSQLiteOpenHelper(ctx, "Database", null, 2) {

    companion object {
        private var sProvider: DatabaseProvider? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseProvider {
            if (sProvider == null) {
                sProvider = DatabaseProvider(ctx.applicationContext)
            }
            return sProvider!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTables()
    }

    override fun onUpgrade(db: SQLiteDatabase,
                           oldVersion: Int,
                           newVersion: Int) {
        db.dropTables()
        db.createTables()
    }
}

// Access property for Context
val Context.database: DatabaseProvider
    get() = DatabaseProvider.getInstance(applicationContext)