package rutmap.ba.rutmap.data.local.table

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

object PlacesTable : BaseTable() {

    override val NAME = "objects"

    override fun createWith(db: SQLiteDatabase) {
        db.createTable(NAME, true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "naziv" to TEXT,
                "nazivEn" to TEXT,
                "alias" to TEXT,
                "description" to TEXT,
                "descriptionEn" to TEXT,
                "category" to TEXT,
                "categoryParent" to TEXT,
                "categoryEn" to TEXT,
                "categoryParentEn" to TEXT,
                "cat_id" to INTEGER,
                "parentCatId" to INTEGER,
                "coords" to TEXT,
                "lat" to TEXT,
                "lon" to TEXT,
                "centar1" to TEXT,
                "centar2" to TEXT,
                "centar3" to TEXT,
                "centar4" to TEXT,
                "opSlika" to TEXT,
                "opImgUrl" to TEXT,
                "opImgUrl2" to TEXT,
                "opImgUrl3" to TEXT,
                "objectClassImg" to TEXT,
                "telefon" to TEXT,
                "mail" to TEXT,
                "web" to TEXT,
                "slika" to TEXT,
                "slider" to TEXT,
                "address" to TEXT,
                "street_id" to TEXT,
                "people" to TEXT,
                "rating" to TEXT,
                "categoryClass" to TEXT,
                "vrsta" to TEXT,
                "streetNumber" to TEXT,
                "minZoom" to TEXT,
                "package" to INTEGER,
                "showOnSearch" to TEXT,
                "tags" to TEXT,
                "tagsEn" to TEXT,
                "workTime" to TEXT,

                "ulica" to TEXT,
                "ulica_id" to TEXT,
                "thumbSlika" to TEXT,
                "glavna" to TEXT,
                "glasovi" to TEXT,
                "ljudi" to TEXT,
                "kategorijaParent" to TEXT,
                "kategorija" to TEXT,
                "kategorijaParentEn" to TEXT,
                "kategorijaEn" to TEXT
        )
    }
}