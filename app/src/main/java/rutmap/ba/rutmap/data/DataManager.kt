package rutmap.ba.rutmap.data

import android.content.Context
import io.reactivex.Observable
import io.reactivex.Observable.fromCallable
import rutmap.ba.rutmap.data.model.*
import rutmap.ba.rutmap.data.remote.loadRemoteEvents
import rutmap.ba.rutmap.data.remote.loadRemotePlaces
import rutmap.ba.rutmap.data.remote.loadRemoteStreets
import rutmap.ba.rutmap.util.data.*

class DataManager(val ctx: Context) {

    fun loadRemoteEventsObs(): Observable<List<Event>> {
        return fromCallable { loadRemoteEvents() }
    }

    fun loadLocalEventsObs(): Observable<List<Event>> {
        return fromCallable { loadLocalEvents() }
    }

    fun loadPlacesObs(): Observable<List<Place>> {
        return fromCallable { loadRemotePlaces() }
    }

    fun loadStreetsObs(): Observable<List<Street>> {
        return fromCallable { loadRemoteStreets() }
    }

    fun loadEvent(id: Int): Observable<Event> {
        return fromCallable { loadLocalEvent(id) }
    }

    fun searchEvents(input: String): Observable<List<Event>> {
        return fromCallable { searchLocalEvents(input) }
    }

    fun loadCategories(placeList: List<Place>? = null)
            : Observable<List<Category>> {
        return fromCallable { loadLocalCategories(placeList) }
    }

    fun loadEventCategory(id: Int): Observable<EventCategory> {
        return fromCallable { loadLocalEventCategory(id) }
    }

    fun loadPlaceCategory(id: Int): Observable<Category> {
        return fromCallable { loadLocalPlaceCategory(id) }
    }

    fun loadSlider(id: Int): Observable<Slider> {
        return fromCallable { loadLocalSlider(id) }
    }

    fun loadStreets(): Observable<List<Street>> {
        return fromCallable { loadLocalStreets() }
    }

    fun searchStreets(input: String): Observable<List<Street>> {
        return fromCallable { searchLocalStreets(input) }
    }

    fun loadStreet(id: Int): Observable<Street> {
        return fromCallable { loadLocalStreet(id) }
    }

    fun loadGuidePlaces(guide: Guide): Observable<List<Place>> {
        return fromCallable { loadLocalGuidePlaces(guide) }
    }

    fun loadPlace(id: Int): Observable<Place> {
        return fromCallable { loadLocalPlace(id) }
    }

    fun loadPlace(name: String): Observable<Place> {
        return fromCallable { loadLocalPlace(name) }
    }

    fun loadPlace(lat: Double): Observable<Place> {
        return fromCallable { loadLocalPlace(lat) }
    }

    fun loadPlaces(category: Category? = null,
                   isOnlyPremium: Boolean = true,
                   onlyRecommended: Boolean = false,
                   isAd: Boolean = false): Observable<List<Place>> {
        return fromCallable {
            loadLocalPlaces(
                    category,
                    isOnlyPremium,
                    onlyRecommended,
                    isAd)
        }
    }

    fun loadPlaces(ids: List<Int>): Observable<List<Place>> {
        return fromCallable { loadLocalPlaces(ids) }
    }

    fun searchPlaces(input: String): Observable<List<Place>> {
        return fromCallable { searchLocalPlaces(input.toUpperCase()) }
    }

    fun isBookmarked(objectId: Int): Observable<Boolean> {
        return fromCallable { isLocalBookmarked(objectId) }
    }

    fun saveBookmark(bookmark: Bookmark) {
        return saveLocalBookmark(bookmark)
    }

    fun deleteBookmark(bookmark: Bookmark) {
        return deleteLocalBookmark(bookmark)
    }

    fun loadBookmarks(): Observable<List<Bookmark>> {
        return fromCallable { loadLocalBookmarks() }
    }
}