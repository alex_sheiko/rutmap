package rutmap.ba.rutmap.data.local.table

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

object EventsTable : BaseTable() {

    override val NAME = "events"

    override fun createWith(db: SQLiteDatabase) {
        db.createTable(NAME, true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "title" to TEXT,
                "titleEn" to TEXT,
                "objectId" to INTEGER,
                "alias" to TEXT,
                "aliasEn" to TEXT,
                "details" to TEXT,
                "detailsEn" to TEXT,
                "mainPic" to TEXT + DEFAULT("'/look/events/DesavanjaDefault.jpg'"),
                "bigimg" to TEXT,
                "tel" to TEXT,
                "eventTime" to TEXT,
                "catId" to INTEGER,
                "email" to TEXT,
                "published" to INTEGER + DEFAULT("0"),
                "sff" to INTEGER + DEFAULT("0"),
                "spec" to INTEGER + DEFAULT("0"))
    }
}