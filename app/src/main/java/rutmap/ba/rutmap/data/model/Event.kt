package rutmap.ba.rutmap.data.model

import io.mironov.smuggler.AutoParcelable

data class Event(
        val id: Int,
        val title: String,
        val titleEn: String,
        val objectId: Int,
        val alias: String,
        val aliasEn: String,
        val details: String?,
        val detailsEn: String?,
        val mainPic: String,
        val bigimg: String?,
        val tel: String?,
        val eventTime: String,
        val catId: Int,
        val email: String?,
        val published: Int,
        val sff: Int,
        val spec: Int
) : AutoParcelable