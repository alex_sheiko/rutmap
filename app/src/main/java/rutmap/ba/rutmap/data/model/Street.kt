package rutmap.ba.rutmap.data.model

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable
import rutmap.ba.rutmap.data.remote.BuildingsResponse

@SuppressLint("ParcelCreator")
data class Street(
        val id: Int,
        val name: String,
        val full_name: String,
        val alias: String,
        val coords: String,
        val polilines: String,
        val municipality_id: String?,
        val points: BuildingsResponse
) : AutoParcelable