package rutmap.ba.rutmap.data.local.table

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.dropTable

abstract class BaseTable {

    abstract val NAME: String

    abstract fun createWith(db: SQLiteDatabase)

    fun dropWith(db: SQLiteDatabase) {
        db.dropTable(NAME, true)
    }
}