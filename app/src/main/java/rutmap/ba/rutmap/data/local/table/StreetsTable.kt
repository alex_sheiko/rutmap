package rutmap.ba.rutmap.data.local.table

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

object StreetsTable : BaseTable() {

    override val NAME = "streets"

    override fun createWith(db: SQLiteDatabase) {
        db.createTable(NAME, true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "name" to TEXT,
                "full_name" to TEXT,
                "alias" to TEXT,
                "coords" to TEXT,
                "polilines" to TEXT,
                "municipality_id" to TEXT,
                "points" to TEXT)
    }
}