package rutmap.ba.rutmap.data.remote

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface ApiPlacesStreets {

    @GET("poi.json")
    fun getPlacesResponse(): Call<ResponseBody>

    @GET("streets.json")
    fun getStreetsResponse(): Call<ResponseBody>
}