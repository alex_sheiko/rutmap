package rutmap.ba.rutmap.data.remote

import rutmap.ba.rutmap.data.model.Street

data class StreetsResponse(val data: List<Street>)