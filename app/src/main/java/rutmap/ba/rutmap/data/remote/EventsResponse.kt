package rutmap.ba.rutmap.data.remote

import rutmap.ba.rutmap.data.model.Event

data class EventsResponse(val data: List<Event>)