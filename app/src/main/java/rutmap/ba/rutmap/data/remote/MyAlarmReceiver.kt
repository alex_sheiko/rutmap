package rutmap.ba.rutmap.data.remote

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.jetbrains.anko.startService

class MyAlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        context.startService<SyncService>()
    }

    companion object {
        val REQUEST_CODE = 12345
    }
}
