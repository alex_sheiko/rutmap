package rutmap.ba.rutmap.data.local.table

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

object SlidersTable : BaseTable() {

    override val NAME = "slider"

    override fun createWith(db: SQLiteDatabase) {
        db.createTable(NAME, true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "object_id" to INTEGER,
                "image" to TEXT,
                "sort" to INTEGER)
    }
}