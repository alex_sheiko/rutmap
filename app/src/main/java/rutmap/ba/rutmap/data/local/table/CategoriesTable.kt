package rutmap.ba.rutmap.data.local.table

import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

object CategoriesTable : BaseTable() {

    override val NAME = "categories"

    override fun createWith(db: SQLiteDatabase) {
        db.createTable(NAME, true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "name" to TEXT,
                "parent_id" to INTEGER + DEFAULT("0"),
                "params" to TEXT,
                "status" to INTEGER + DEFAULT("1"),
                "classname" to TEXT,
                "html" to TEXT,
                "alias" to TEXT,
                "lang_del" to INTEGER + DEFAULT("1"),
                "nameLatin" to TEXT,
                "nameEn" to TEXT,
                "nameLatinEn" to TEXT,
                "aliasEn" to TEXT)
    }
}