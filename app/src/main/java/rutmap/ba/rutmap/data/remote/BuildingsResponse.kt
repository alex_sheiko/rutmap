package rutmap.ba.rutmap.data.remote

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable
import rutmap.ba.rutmap.data.model.StreetBuilding

@SuppressLint("ParcelCreator")
data class BuildingsResponse(
        val ulice: List<StreetBuilding>
) : AutoParcelable