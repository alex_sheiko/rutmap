package rutmap.ba.rutmap.data.model

import android.annotation.SuppressLint
import io.mironov.smuggler.AutoParcelable

@SuppressLint("ParcelCreator")
data class StreetBuilding(
        val streetnumber: String,
        val street: String,
        val lat: String,
        val lng: String,
        val subnumber: String,
        val street_id: Int
) : AutoParcelable