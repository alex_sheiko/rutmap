package rutmap.ba.rutmap.data.model

import io.mironov.smuggler.AutoParcelable

data class Category(
        val id: Int,
        val name: String,
        val parent_id: Int,
        val params: String,
        val status: Int,
        val classname: String,
        val html: String,
        val alias: String,
        val lang_del: Int,
        val nameLatin: String,
        val nameEn: String,
        val nameLatinEn: String,
        val aliasEn: String
) : AutoParcelable