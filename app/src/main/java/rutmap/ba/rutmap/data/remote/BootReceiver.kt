package rutmap.ba.rutmap.data.remote

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if ("android.intent.action.BOOT_COMPLETED" == intent.action) {
            val i = Intent(context, SyncService::class.java)
            context.startService(i)
        }
    }
}
