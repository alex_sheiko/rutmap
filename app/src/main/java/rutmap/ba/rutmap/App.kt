package rutmap.ba.rutmap

import android.support.multidex.MultiDexApplication
import rutmap.ba.rutmap.util.design.initDependencies

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        initDependencies()
    }
}