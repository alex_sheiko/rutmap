package rutmap.ba.rutmap.util.design

import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.liulishuo.filedownloader.FileDownloader
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.services.android.telemetry.constants.TelemetryConstants
import com.mapbox.services.android.telemetry.utils.TelemetryUtils
import io.fabric.sdk.android.Fabric
import rutmap.ba.rutmap.App
import rutmap.ba.rutmap.BuildConfig
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.util.map.offline.OfflineFileManager
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyConfig.initDefault

fun App.initDependencies() {
    initCustomFont()
    initFileManager()
    initCrashlytics()
    initMapbox()
}

fun initCustomFont() {
    initDefault(CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/Geomanist-Regular.otf")
            .setFontAttrId(R.attr.fontPath)
            .build())
}


fun App.initFileManager() {
    FileDownloader.init(applicationContext)
    OfflineFileManager.init(applicationContext)
}

fun App.initMapbox() {
    Mapbox.getInstance(this, "_")

    // Fixes crash when downloading audio with my location enabled
    TelemetryUtils.getSharedPreferences(this)
            .edit()
            .putBoolean(TelemetryConstants.MAPBOX_SHARED_PREFERENCE_KEY_TELEMETRY_ENABLED, false)
            .apply()
}

fun App.initCrashlytics() {
    // Set up Crashlytics, disabled for debug builds
    val crashlyticsKit = Crashlytics.Builder()
            .core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
            .build()

    // Initialize Fabric with the debug-disabled crashlytics.
    Fabric.with(this, crashlyticsKit)
}