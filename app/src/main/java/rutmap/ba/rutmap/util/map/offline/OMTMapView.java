package rutmap.ba.rutmap.util.map.offline;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMapOptions;

public class OMTMapView extends MapView {
    private Style style;
    private AttributionOnClickListener attributionOnClickListener;

    public OMTMapView(@NonNull Context context) {
        super(context);
        attributionOnClickListener = new AttributionOnClickListener();
        //        if(attrView != null) {
        //            attrView.setOnClickListener(attributionOnClickListener);
        //        }
    }

    public OMTMapView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        attributionOnClickListener = new AttributionOnClickListener();
        //        if(attrView != null) {
        //            attrView.setOnClickListener(attributionOnClickListener);
        //        }
    }

    public OMTMapView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        attributionOnClickListener = new AttributionOnClickListener();
        //        if(attrView != null) {
        //            attrView.setOnClickListener(attributionOnClickListener);
        //        }
    }

    public OMTMapView(@NonNull Context context, @Nullable MapboxMapOptions options) {
        super(context, options);
        attributionOnClickListener = new AttributionOnClickListener();
        //        if(attrView != null) {
        //            attrView.setOnClickListener(attributionOnClickListener);
        //        }
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
        setStyleJSON(style.getStyleJson());
    }

    /**
     * Changes displayed map, to use provided style, will also create Style object
     * that can be retrieved with getStyle method
     * @param url The URL of the map style
     */
    @Override
    public void setStyleUrl(String url) {
        if (url != null) {
            // Preprocess the style, so native code receives only absolute paths
            if (url.startsWith(Consts.SCHEME_ASSET)) {
                String filename = url.substring(Consts.SCHEME_ASSET.length());
                style = Style.createFromAsset(getContext(), filename);
                reapplyStyle();

            } else {
                super.setStyleUrl(url);
            }
        }
    }

    public void reapplyStyle() {

        //  mStyleInitializer.setStyle(getStyleUrl());
        setStyle(style);
    }

    private class AttributionOnClickListener implements View.OnClickListener, DialogInterface.OnClickListener {
        private static final int ATTRIBUTION_INDEX_ABOUT_THIS = 0;
        private static final int ATTRIBUTION_INDEX_KLOKANTECH = 1;
        private static final int ATTRIBUTION_INDEX_MAPBOX = 2;
        private static final int ATTRIBUTION_INDEX_OPENSTREETMAP = 3;
        private static final int ATTRIBUTION_INDEX_MAP_ATTRIB = 4;

        public AttributionOnClickListener() {
            super();
        }

        // Called when someone presses the attribution icon
        @Override
        public void onClick(View v) {
        }

        // Called when someone selects an attribution, 'Improve this map' adds location data to the url
        @Override
        public void onClick(DialogInterface dialog, int which) {
        }
    }



}
