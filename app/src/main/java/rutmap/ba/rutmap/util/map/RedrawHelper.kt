package rutmap.ba.rutmap.util.map

import android.content.Context
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.maps.MapboxMap
import rutmap.ba.rutmap.data.model.Category

var ZOOM_BREAKPOINT = 15

var onlyPremiumShown: Boolean = true
var freePlacesShown: Boolean = false

var famousShownSmall: Boolean = true
var famousShownBig: Boolean = false

fun MapboxMap.setRedrawListener(ctx: Context,
                                parentCategory: Category?,
                                isRecommended: Boolean,
                                isAd: Boolean = false) {
    setOnCameraChangeListener {
        handleFamous(ctx, it)
        handleRegular(ctx, it, parentCategory, isRecommended, isAd)

        mPreviousZoom = it.zoom
    }
}

private fun MapboxMap.handleFamous(ctx: Context, it: CameraPosition) {
    val timeToRedrawFamous: Boolean

    if (it.zoom >= ZOOM_BREAKPOINT && !famousShownBig) {
        famousShownBig = true
        famousShownSmall = false
        timeToRedrawFamous = true
    } else if (it.zoom < ZOOM_BREAKPOINT && !famousShownSmall) {
        famousShownBig = false
        famousShownSmall = true
        timeToRedrawFamous = true
    } else {
        timeToRedrawFamous = false
    }

    if (timeToRedrawFamous) {
        if (mPreviousZoom != 0.0) {
            clear()
            showMarkersFamous(ctx, it.zoom >= ZOOM_BREAKPOINT)
        }
    }
}

private fun MapboxMap.handleRegular(
        ctx: Context,
        it: CameraPosition,
        parentCategory: Category?,
        isRecommended: Boolean,
        isAd: Boolean = false) {
    val timeToRedrawRegular: Boolean

    if (it.zoom >= ZOOM_BREAKPOINT && !freePlacesShown) {
        freePlacesShown = true
        onlyPremiumShown = false
        timeToRedrawRegular = true
    } else if (it.zoom < ZOOM_BREAKPOINT && !onlyPremiumShown) {
        freePlacesShown = false
        onlyPremiumShown = true
        timeToRedrawRegular = true
    } else {
        timeToRedrawRegular = false
    }

    if (timeToRedrawRegular) {
        if (mPreviousZoom != 0.0) {
            showMarkersPlaces(
                    ctx,
                    parentCategory,
                    it.zoom < ZOOM_BREAKPOINT,
                    isRecommended,
                    isAd)
        }
    }
}