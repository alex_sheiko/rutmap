package rutmap.ba.rutmap.util.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import rutmap.ba.rutmap.data.local.table.*

fun SQLiteDatabase.createTables() {
    EventsTable.createWith(this)
    CategoriesTable.createWith(this)
    EventCategoriesTable.createWith(this)
    SlidersTable.createWith(this)
    StreetsTable.createWith(this)
    PlacesTable.createWith(this)
    BookmarksTable.createWith(this)
}

fun SQLiteDatabase.dropTables() {
    EventsTable.dropWith(this)
    CategoriesTable.dropWith(this)
    EventCategoriesTable.dropWith(this)
    SlidersTable.dropWith(this)
    StreetsTable.dropWith(this)
    PlacesTable.dropWith(this)
    BookmarksTable.dropWith(this)
}

fun SQLiteDatabase.insertData(ctx: Context) {
    insertCategories(ctx)
    insertEventCategories(ctx)
    insertSliders(ctx)
    insertStreets(ctx)
    insertPlaces(ctx)
}