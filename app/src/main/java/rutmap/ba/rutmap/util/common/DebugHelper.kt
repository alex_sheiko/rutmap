package rutmap.ba.rutmap.util.common

import android.app.Activity
import org.jetbrains.anko.toast

fun Activity.comingSoon() {
    toast("Coming soon")
}

/*
fun App.detectMemoryLeaks() {
    if (LeakCanary.isInAnalyzerProcess(this)) {
        // This process is dedicated to LeakCanary for heap analysis.
        return
    }
    LeakCanary.install(this)
}
*/