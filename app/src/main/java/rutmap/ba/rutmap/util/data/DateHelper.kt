package rutmap.ba.rutmap.util.data

import java.text.SimpleDateFormat
import java.util.*
import java.util.Locale.getDefault

private val inputFormat
        = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", getDefault())
private val outputFormat
        = SimpleDateFormat("dd.MM.yyyy - HH:mm'h'", getDefault())

fun formatTime(input: String): String {
    val subject = input.convertToDate()
    return outputFormat.format(subject)
}

fun String.convertToDate(): Date {
    return inputFormat.parse(this)
}

fun String.isAfterNow(): Boolean {
    val subject = this.convertToDate()
    val now = Date()
    return subject.time > now.time
}

fun Date.isSameDay(date2: Date): Boolean {
    val date1 = this
    val fmt = SimpleDateFormat("yyyyMMdd", getDefault())
    return fmt.format(date1) == fmt.format(date2)
}

fun today(): Date {
    return Date()
}