package rutmap.ba.rutmap.util.data.model

import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.util.common.Constants.AD_PLACE_IDS

fun Category.isParent(): Boolean {
    return parent_id == 0
}

fun createChildQuery(children: List<Category>): String {
    val idList = ArrayList<Int>()
    children.forEach { idList.add(it.id) }
    return "(" + idList.joinToString(" OR cat_id = ", "cat_id = ") + ")"
}

fun createAdQuery(): String {
    val idList = AD_PLACE_IDS
    return "(" + idList.joinToString(" OR id = ", "id = ") + ")"
}

fun createPlacesQuery(ids: List<Int>): String {
    val idList = ArrayList<Int>()
    ids.forEach { idList.add(it) }
    return "(" + idList.joinToString(" OR id = ", "id = ") + ")"
}

fun createCategoriesQuery(placeList: List<Place>): String {
    val idList = ArrayList<Int>()
    placeList.forEach { idList.add(it.cat_id) }
    return "(" + idList.distinct().joinToString(" OR id = ", "id = ") + ")"
}

fun Category.getColorKey(): String {
    return name.substring(0, 1)
}