package rutmap.ba.rutmap.util.data

import android.content.Context
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

fun Context.readSqlCommands(filename: String): String {
    return openFile(filename)
            .readString()
            .escapeQuotes()
            .removeSpacesBefore()
}

fun Context.openFile(filename: String): InputStream {
    return assets.open(filename)
}

private fun InputStream.readString(): String {
    val buf = StringBuilder()

    val `in` = BufferedReader(InputStreamReader(this, "UTF-8"))

    while (true) {
        val str = `in`.readLine()
        if (str == null) {
            `in`.close()
            return buf.toString()
        }
        buf.append(str)
    }
}