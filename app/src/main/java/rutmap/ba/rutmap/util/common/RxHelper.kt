package rutmap.ba.rutmap.util.common

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers.io

fun <T> Observable<T>.async(): Observable<T> {
    return subscribeOn(io())
            .observeOn(mainThread())
}