package rutmap.ba.rutmap.util.common

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri.parse
import java.io.File

fun Context.createMediaPlayer(audioFile: File): MediaPlayer {
    val uri = parse(audioFile.absolutePath)

    return MediaPlayer.create(this, uri)
}