package rutmap.ba.rutmap.util.design

import android.graphics.drawable.Drawable
import android.widget.TextView

fun TextView.setDrawableLeft(drawable: Drawable) {
    drawable.setBounds(0, 0, 30, 30)
    setCompoundDrawables(drawable, null, null, null)
}

fun TextView.setDrawableRight(drawable: Drawable) {
    drawable.setBounds(0, 0, 15, 15)
    setCompoundDrawables(null, null, drawable, null)
}