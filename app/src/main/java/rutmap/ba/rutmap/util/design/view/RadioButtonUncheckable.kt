package rutmap.ba.rutmap.util.design.view

import android.content.Context
import android.util.AttributeSet
import android.widget.RadioButton
import android.widget.RadioGroup

class RadioButtonUncheckable(ctx: Context, attrs: AttributeSet)
    : RadioButton(ctx, attrs) {

    override fun toggle() {
        if (isChecked) {
            if (parent != null && parent is RadioGroup) {
                (parent as RadioGroup).clearCheck()
            }
        } else {
            isChecked = true
        }
    }
}
