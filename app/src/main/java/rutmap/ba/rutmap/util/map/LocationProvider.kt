package rutmap.ba.rutmap.util.map

import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices.API
import com.google.android.gms.location.LocationServices.FusedLocationApi
import rutmap.ba.rutmap.util.data.getSavedLocation
import rutmap.ba.rutmap.util.data.saveLocation
import java.lang.Math.*
import java.text.DecimalFormat

var mGoogleApiClient: GoogleApiClient? = null

fun Fragment.getLocation(callback: (Location?) -> Unit) {
    if (context.getSavedLocation() != null) {
        callback.invoke(context.getSavedLocation())
        return
    }

    if (mGoogleApiClient == null) {
        mGoogleApiClient = GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(connectionHint: Bundle?) {
                        retrieveLastLocation(callback)
                    }

                    override fun onConnectionSuspended(cause: Int) {
                    }
                })
                .addOnConnectionFailedListener { callback.invoke(null) }
                .addApi(API)
                .build()
        mGoogleApiClient!!.connect()
    } else {
        retrieveLastLocation(callback)
    }
}

fun Location?.getDistanceTo(destination: Location): String {
    if (this == null) {
        return "—"
    }
    val lat1 = latitude
    val lon1 = longitude
    val lat2 = destination.latitude
    val lon2 = destination.longitude

    val theta = lon1 - lon2
    var dist = sin(deg2rad(lat1)) *
            sin(deg2rad(lat2)) +
            cos(deg2rad(lat1)) *
                    cos(deg2rad(lat2)) *
                    cos(deg2rad(theta))
    dist = acos(dist)
    dist = rad2deg(dist)
    dist *= 60.0 * 1.1515

    val distance = DecimalFormat("#.#").format(dist)

    return "$distance km"
}

private fun Fragment.retrieveLastLocation(callback: (Location?) -> Unit) {
    val myLocation = FusedLocationApi.getLastLocation(
            mGoogleApiClient)
    callback.invoke(myLocation)

    if (myLocation != null) {
        context.saveLocation(myLocation)
    }
}

private fun deg2rad(deg: Double): Double {
    return deg * Math.PI / 180.0
}

private fun rad2deg(rad: Double): Double {
    return rad * 180.0 / Math.PI
}