package rutmap.ba.rutmap.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.Config.ARGB_8888
import android.graphics.BitmapFactory.decodeResource
import android.graphics.Canvas
import android.graphics.Color.WHITE
import android.graphics.Paint
import android.graphics.Paint.Align.CENTER
import android.graphics.Paint.Style.FILL
import rutmap.ba.rutmap.util.common.spToPixels

fun Context.writeOnDrawable(drawableId: Int,
                            text: String,
                            color: Int = WHITE): Bitmap {

    val bm = decodeResource(resources, drawableId)
            .copy(ARGB_8888, true)

    val paint = Paint()
    paint.style = FILL
    paint.color = color
    paint.textSize = spToPixels(10)
    paint.textAlign = CENTER

    val canvas = Canvas(bm)
    canvas.drawText(
            text,
            (canvas.width / 2).toFloat(),
            ((canvas.height / 2)
                    - ((paint.descent()
                    + paint.ascent()) / 2)),
            paint)

    return bm
}