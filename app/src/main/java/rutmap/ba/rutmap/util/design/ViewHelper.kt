package rutmap.ba.rutmap.util.design

import android.view.View
import android.view.ViewGroup

fun View.getChildren(): List<View> {
    if (this !is ViewGroup) {
        val viewArrayList = ArrayList<View>()
        viewArrayList.add(this)
        return viewArrayList
    }

    val result = ArrayList<View>()

    val viewGroup = this
    for (i in 0..viewGroup.childCount - 1) {

        val child = viewGroup.getChildAt(i)

        val viewArrayList = ArrayList<View>()
        viewArrayList.addAll(child.getChildren())

        result.addAll(viewArrayList)
    }
    return result
}