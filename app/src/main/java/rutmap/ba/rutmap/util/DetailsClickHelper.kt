package rutmap.ba.rutmap.util

import android.app.Activity
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Guide
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.ui.eventdetail.EventDetailActivity
import rutmap.ba.rutmap.ui.guidedetail.GuideDetailActivity
import rutmap.ba.rutmap.ui.placedetail.PlaceDetailActivity
import rutmap.ba.rutmap.ui.streetdetail.StreetMapActivity

fun Any.navigateToDetails(ctx: Any) {
    val activity = ctx as Activity

    if (this is Place) {
        activity.startActivity<PlaceDetailActivity>(
                "place" to this
        )
    } else if (this is Street) {
        activity.startActivity<StreetMapActivity>(
                "street" to this
        )
    } else if (this is Event) {
        activity.startActivity<EventDetailActivity>(
                "event" to this
        )
    } else if (this is Guide) {
        activity.startActivity<GuideDetailActivity>(
                "guide" to this
        )
    }
}