package rutmap.ba.rutmap.util.common

import android.app.Activity
import android.content.Context
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.ui.main.MainActivity
import rutmap.ba.rutmap.util.data.saveLocale
import rutmap.ba.rutmap.util.data.setUserNotNew

fun Context.getAppVersion(): String {
    val info = packageManager.getPackageInfo(packageName, 0)
    return info.versionName
}

fun Activity.restart(lang: String) {
    finish()
    saveLocale(lang)
    startActivity(intent)
}

fun Activity.goHome(lang: String) {
    setUserNotNew()
    saveLocale(lang)

    finishAffinity()
    startActivity<MainActivity>()
}