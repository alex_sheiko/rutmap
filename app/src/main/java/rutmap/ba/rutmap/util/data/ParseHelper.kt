package rutmap.ba.rutmap.util.data

import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.data.remote.BuildingsResponse

fun createEmptyEvent(): Event {
    return Event(0, "", "", 0, "", "", "", "", "", "", "", "", 0, "", 0, 0, 0)
}

fun createEmptyPlace(): Place {
    return Place(0, "", "", "", "", "", "", "", "", "", 0, 0, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, "", "", "", "")
}

fun createEmptyStreet(): Street {
    return Street(0, "", "", "", "", "", "", BuildingsResponse(emptyList()))
}

fun createEmptyCategory(): Category {
    return Category(0, "", 0, "", 0, "", "", "", 0, "", "", "", "other-places")
}