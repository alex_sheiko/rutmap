package rutmap.ba.rutmap.util.map

import android.content.Context
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.applyDimension

fun Int.dpToFixels(ctx: Context): Int {
    return applyDimension(
            COMPLEX_UNIT_DIP,
            this.toFloat(),
            ctx.resources.displayMetrics).toInt()
}