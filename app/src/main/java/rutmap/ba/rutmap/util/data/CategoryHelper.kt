package rutmap.ba.rutmap.util.data

import rutmap.ba.rutmap.data.model.Category

val categoryPlacesMap: Map<String, List<Int>> by lazy {
    val map = HashMap<String, List<Int>>()
    map.put("food and drinks", listOf(1126, 241, 3563, 3155, 329, 2376))
    map.put("night life", listOf(1632, 1565, 1644, 309, 110, 1563))
    map.put("accommodation", listOf(5280, 10, 1440, 1569, 586, 5793))
    map.put("shops and services", listOf(6151, 1658, 1124, 1650, 2539))
    map.put("tourist agency", listOf(154, 204, 571, 1132))
    map.put("parks and attractions", listOf(5288, 14, 1049, 1123, 1050, 1051, 1218))
    map.put("arts and entertainment", listOf(100, 1145, 3882, 3542, 174, 169))
    map.put("education", emptyList())
    map
}

fun Category.getPremiumPlaces(): List<Int> {
    return categoryPlacesMap.getValue(nameLatinEn)
}