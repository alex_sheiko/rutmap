package rutmap.ba.rutmap.util.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import org.jetbrains.anko.db.*
import org.jetbrains.anko.db.SqlOrderDirection.DESC
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.local.database
import rutmap.ba.rutmap.data.local.table.*
import rutmap.ba.rutmap.data.model.*
import rutmap.ba.rutmap.data.remote.PlacesResponse
import rutmap.ba.rutmap.data.remote.StreetsResponse
import rutmap.ba.rutmap.util.data.model.*
import java.io.IOException
import java.io.StringReader

fun DataManager.loadLocalEvents(): List<Event> {
    var result = emptyList<Event>()
    ctx.database.use {
        select(EventsTable.NAME)
                .orderBy("eventTime")
                .exec {
                    result = parseList(rowParser(::Event))
                }
    }
    return result
}

fun DataManager.searchLocalEvents(input: String): List<Event> {
    var result = emptyList<Event>()
    ctx.database.use {
        select(EventsTable.NAME)
                .where("UPPER(title) LIKE '%$input%' " +
                        " OR UPPER(titleEn) LIKE '%$input%'")
                .exec {
                    result = parseList(rowParser(::Event))
                }
    }
    return result
}

fun DataManager.loadLocalCategories(
        placeList: List<Place>? = null): List<Category> {

    var result = emptyList<Category>()
    ctx.database.use {
        val query = select(CategoriesTable.NAME)
        if (placeList != null) {
            query.where(createCategoriesQuery(placeList))
        }
        query.orderBy("parent_id, name")
        query.exec {
            result = parseList(rowParser(::Category))
        }
    }
    return result
}

fun DataManager.loadLocalEventCategory(id: Int): EventCategory? {
    var result: EventCategory? = null
    ctx.database.use {
        select(EventCategoriesTable.NAME)
                .where("id = $id")
                .exec {
                    result = parseSingle(rowParser(::EventCategory))
                }
    }
    return result
}

fun DataManager.loadLocalPlaceCategory(id: Int): Category {
    var result: Category? = null
    ctx.database.use {
        select(CategoriesTable.NAME)
                .where("id = $id")
                .exec {
                    result = parseOpt(rowParser(::Category))
                            ?: createEmptyCategory()
                }
    }
    return result!!
}

fun DataManager.loadLocalSlider(id: Int): Slider {
    var result: Slider? = null
    ctx.database.use {
        select(SlidersTable.NAME)
                .where("object_id = {id}", "id" to id)
                .limit(1)
                .exec {
                    result = parseOpt(rowParser(::Slider))
                            ?: Slider(0, 0, "", 0)
                }
    }
    return result!!
}

fun DataManager.loadLocalStreets(): List<Street> {
    var result = emptyList<Street>()
    ctx.database.use {
        select(StreetsTable.NAME)
                .exec {
                    try {
                        val buildingsType = object : TypeToken<ArrayList<StreetBuilding>>() {}.type
                        result = parseList(StreetsParser(buildingsType, Gson(), true))
                    } catch (e: SQLiteException) {
                        e.printStackTrace()
                    }
                }
    }
    return result
}

fun DataManager.searchLocalStreets(input: String): List<Street> {
    var result = emptyList<Street>()
    ctx.database.use {
        select(StreetsTable.NAME)
                .where("UPPER(name) LIKE '%$input%'" +
                        " OR UPPER(full_name) LIKE '%$input%'")
                .exec {
                    val buildingsType = object : TypeToken<ArrayList<StreetBuilding>>() {}.type
                    result = parseList(StreetsParser(buildingsType, Gson(), true))
                }
    }
    return result
}

fun DataManager.loadLocalPlaces(ids: List<Int>): List<Place> {
    val result = ArrayList<Place>()
    ctx.database.use {
        val query = select(PlacesTable.NAME)
        query.where(createPlacesQuery(ids))
        query.exec {
            if (moveToFirst()) {
                do {
                    result.add(retrievePlace())
                } while (moveToNext())
            }
        }
    }
    return result
}

fun DataManager.loadLocalPlaces(category: Category? = null,
                                onlyPremium: Boolean,
                                onlyRecommended: Boolean = false,
                                isAd: Boolean = false): List<Place> {
    val result = ArrayList<Place>()
    ctx.database.use {
        val query = select(PlacesTable.NAME)
        var whereQuery: String = ""
        if (category != null) {
            if (category.isParent()) {
                whereQuery = createChildQuery(
                        getChildren(category))
            } else {
                whereQuery = "cat_id = $category"
            }
            if (onlyPremium) {
                whereQuery = "$whereQuery AND"
            }
        }
        if (onlyPremium && !isAd) {
            whereQuery = "$whereQuery package > 1"
        }
        if (onlyRecommended) {
            if (whereQuery.contains("package > 1")) {
                whereQuery = whereQuery.replace("package > 1", "package > 2")
            } else {
                whereQuery = "$whereQuery package > 2"
            }
        }
        if (isAd) {
            whereQuery = createAdQuery()
        }
        if (whereQuery.isNotEmpty()) {
            query.where("($whereQuery)")
        }
        try {
            query.orderBy("nazivEn")
            query.exec {
                if (moveToFirst()) {
                    do {
                        result.add(retrievePlace())
                    } while (moveToNext())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    return result
}

fun DataManager.loadLocalGuidePlaces(guide: Guide): List<Place> {
    val result = ArrayList<Place>()
    ctx.database.use {
        val query = select(PlacesTable.NAME)
        query.where(createPlacesQuery(guide.placeArray.toList()))
        query.exec {
            if (moveToFirst()) {
                do {
                    result.add(retrievePlace())
                } while (moveToNext())
            }
        }
    }
    return result
}

fun DataManager.searchLocalPlaces(input: String): List<Place> {
    val result = ArrayList<Place>()

    ctx.database.use {
        val query = select(PlacesTable.NAME)
        query.where("UPPER(naziv) LIKE '%$input%'" +
                " OR UPPER(nazivEn) LIKE '%$input%'" +
                " OR UPPER(tags) LIKE '%$input%'" +
                " OR UPPER(tagsEn) LIKE '%$input%'")
        query.orderBy("package", DESC)
        query.exec {
            if (moveToFirst()) {
                do {
                    val place = retrievePlace()
                    if (place.naziv != "NULA") {
                        result.add(place)
                    }
                } while (moveToNext())
            }
        }
    }
    return result
}

fun DataManager.loadLocalPlace(id: Int): Place? {
    var result: Place? = null
    ctx.database.use {
        try {
            select(PlacesTable.NAME)
                    .where("id = $id")
                    .exec {
                        moveToFirst()
                        result = retrievePlace()
                    }
        } catch (e: Exception) {
            result = createEmptyPlace()
            e.printStackTrace()
        }
    }
    return result
}

fun DataManager.loadLocalPlace(name: String): Place? {
    var result: Place? = null
    ctx.database.use {
        select(PlacesTable.NAME)
                .where("naziv LIKE '$name'")
                .exec {
                    if (moveToFirst()) {
                        result = retrievePlace()
                    } else {
                        result = createEmptyPlace()
                    }
                }
    }
    return result
}

fun DataManager.loadLocalEvent(id: Int): Event {
    var result: Event? = null
    ctx.database.use {
        select(EventsTable.NAME)
                .where("id = $id")
                .exec {
                    result = parseOpt(rowParser(::Event))
                }
    }
    return if (result != null) {
        result!!
    } else {
        createEmptyEvent()
    }
}

fun DataManager.loadLocalPlace(lat: Double): Place? {
    var result: Place? = null
    ctx.database.use {
        select(PlacesTable.NAME)
                .where("lat = '$lat'")
                .exec {
                    if (moveToFirst()) {
                        result = retrievePlace()
                    } else {
                        result = createEmptyPlace()
                    }
                }
    }
    return result
}

fun DataManager.loadLocalStreet(id: Int): Street? {
    var result: Street? = null
    ctx.database.use {
        select(StreetsTable.NAME)
                .where("id = $id")
                .exec {
                    val buildingsType = object : TypeToken<ArrayList<StreetBuilding>>() {}.type
                    result = parseOpt(StreetsParser(buildingsType, Gson()))
                            ?: createEmptyStreet()
                }
    }
    return result
}

fun DataManager.loadLocalCategory(id: Int): Category? {
    var result: Category? = null
    ctx.database.use {
        select(CategoriesTable.NAME)
                .where("id = $id")
                .exec {
                    result = parseOpt(rowParser(::Category))
                }
    }
    return result
}

private fun DataManager.getChildren(
        category: Category): List<Category> {
    var result = emptyList<Category>()
    ctx.database.use {
        select(CategoriesTable.NAME)
                .where("parent_id = ${category.id}")
                .exec {
                    result = parseList(rowParser(::Category))
                }
    }
    return result
}

fun SQLiteDatabase.insertCategories(ctx: Context) {
    val command = ctx.getString(R.string.insert_categories)
    execSQL(command)
}

fun SQLiteDatabase.insertEventCategories(ctx: Context) {
    val command = ctx.getString(R.string.insert_event_categories)
    execSQL(command)
}

fun SQLiteDatabase.insertSliders(ctx: Context) {
    val command = ctx.getString(R.string.insert_sliders)
    execSQL(command)
}

fun SQLiteDatabase.insertStreets(ctx: Context) {
    val json = ctx.loadJSONFromAsset("streets.json")

    val reader = JsonReader(StringReader(json))
    reader.isLenient = true

    val response = Gson().fromJson<StreetsResponse>(
            reader, StreetsResponse::class.java)
    val streetList = response.data

    transaction {
        streetList.forEach {
            val street = it
            insert(StreetsTable.NAME,
                    "id" to street.id,
                    "name" to street.name,
                    "full_name" to street.full_name,
                    "alias" to street.alias,
                    "coords" to street.coords,
                    "polilines" to street.polilines,
                    "municipality_id" to street.municipality_id,
                    "points" to Gson().toJson(street.points))
        }
    }
}

fun SQLiteDatabase.insertPlaces(ctx: Context) {
    val json = ctx.loadJSONFromAsset("places.json")

    val reader = JsonReader(StringReader(json))
    reader.isLenient = true

    val response = Gson().fromJson<PlacesResponse>(
            reader, PlacesResponse::class.java)
    val placeList = response.data

    transaction {
        placeList.forEach {
            val place = it
            insert(PlacesTable.NAME,
                    "id" to place.id,
                    "naziv" to place.naziv,
                    "nazivEn" to place.nazivEn,
                    "alias" to place.alias,
                    "description" to place.description,
                    "descriptionEn" to place.descriptionEn,
                    "category" to place.category,
                    "categoryParent" to place.categoryParent,
                    "categoryEn" to place.categoryEn,
                    "categoryParentEn" to place.categoryParentEn,
                    "cat_id" to place.cat_id,
                    "parentCatId" to place.parentCatId,
                    "coords" to place.coords,
                    "lat" to place.lat,
                    "lon" to place.lon,
                    "centar1" to place.centar1,
                    "centar2" to place.centar2,
                    "centar3" to place.centar3,
                    "centar4" to place.centar4,
                    "opSlika" to place.opSlika,
                    "opImgUrl" to place.opImgUrl,
                    "opImgUrl2" to place.opImgUrl2,
                    "opImgUrl3" to place.opImgUrl3,
                    "objectClassImg" to place.objectClassImg,
                    "telefon" to place.telefon,
                    "mail" to place.mail,
                    "web" to place.web,
                    "slika" to place.slika,
                    "slider" to place.slider,
                    "address" to place.address,
                    "street_id" to place.street_id,
                    "people" to place.people,
                    "rating" to place.rating,
                    "categoryClass" to place.categoryClass,
                    "vrsta" to place.vrsta,
                    "streetNumber" to place.streetNumber,
                    "minZoom" to place.minZoom,
                    "package" to place.`package`,
                    "showOnSearch" to place.showOnSearch,
                    "tags" to place.tags,
                    "tagsEn" to place.tagsEn,
                    "workTime" to place.workTime)
        }
    }
}

fun Context.loadJSONFromAsset(fileName: String): String? {
    val json: String?
    try {
        val `is` = assets.open(fileName)
        val size = `is`.available()
        val buffer = ByteArray(size)
        `is`.read(buffer)
        `is`.close()
        json = String(buffer)
    } catch (ex: IOException) {
        ex.printStackTrace()
        return null
    }
    return json
}

fun DataManager.isLocalBookmarked(objectId: Int): Boolean {
    var result: Boolean = false

    ctx.database.use {
        select(BookmarksTable.NAME)
                .where("id = $objectId")
                .exec {
                    result = count == 1
                }
    }
    return result
}

fun DataManager.saveLocalBookmark(bookmark: Bookmark) {
    ctx.database.use {
        insert(BookmarksTable.NAME,
                "id" to bookmark.id,
                "type" to bookmark.type)
    }
}

fun DataManager.deleteLocalBookmark(bookmark: Bookmark) {
    ctx.database.use {
        delete(BookmarksTable.NAME,
                "id = ${bookmark.id}",
                null)
    }
}

fun DataManager.loadLocalBookmarks(): List<Bookmark> {
    var result = emptyList<Bookmark>()
    ctx.database.use {
        select(BookmarksTable.NAME)
                .exec {
                    result = parseList(rowParser(::Bookmark))
                }
    }
    return result
}