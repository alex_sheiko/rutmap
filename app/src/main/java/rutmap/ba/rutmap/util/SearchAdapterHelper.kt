package rutmap.ba.rutmap.util

import android.content.Context
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.ui.search.SearchAdapter.ViewHolder
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.data.highlight
import rutmap.ba.rutmap.util.data.model.showStreet
import rutmap.ba.rutmap.util.design.image.showIcon

fun ViewHolder.showDetails(ctx: Context, item: Any, query: String) {
    if (item is Place) {
        imageIcon.showIcon(ctx, item)
        textTitle.text = ctx.getLocalized(item, "name").highlight(query)
        textDesc.showStreet(ctx, item)
        textTags.text = ctx.getLocalized(item, "tags").highlight(query)
    } else if (item is Street) {
        textTitle.text = ctx.getLocalized(item, "name").highlight(query)
        textDesc.text = "Street"
        imageIcon.setImageResource(R.drawable.ic_marker_category_other_places_big)
        textTags.text = item.full_name.highlight(query)
    } else if (item is Event) {
        textTitle.text = ctx.getLocalized(item, "name").highlight(query)
        textTitle.text = "Event"
        imageIcon.setImageResource(R.drawable.ic_marker_category_other_places_big)
        textTags.text = ""
    }
}