package rutmap.ba.rutmap.util.common

import android.app.Activity
import rutmap.ba.rutmap.ui.main.showCategory

val FAKE_CATEGORY_FOR_ADS = -1

fun Activity.showAd() {
    showCategory(FAKE_CATEGORY_FOR_ADS)
}