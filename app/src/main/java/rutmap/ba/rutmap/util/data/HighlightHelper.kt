package rutmap.ba.rutmap.util.data

import android.graphics.Color.parseColor
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import java.text.Normalizer

fun String.highlight(search: String): CharSequence {
    // ignore case and accents
    // the same thing should have been done for the search text
    val normalizedText = Normalizer.normalize(this, Normalizer.Form.NFD).replace("\\p{InCombiningDiacriticalMarks}+".toRegex(), "").toLowerCase()

    var start = normalizedText.indexOf(search)
    if (start < 0) {
        // not found, nothing to to
        return this
    } else {
        // highlight each appearance in the original text
        // while searching in normalized text
        val highlighted = SpannableString(this)
        while (start >= 0) {
            val spanStart = Math.min(start, length)
            val spanEnd = Math.min(start + search.length, length)

            highlighted.setSpan(BackgroundColorSpan(parseColor("#64DD17")), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

            start = normalizedText.indexOf(search, spanEnd)
        }

        return highlighted
    }
}