package rutmap.ba.rutmap.util.data

import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Guide

fun getGuideList(): List<Guide> {
    val list = ArrayList<Guide>()
    list.add(Guide(
            "SARAJEVO GRAND TOUR",
            "The city that is commonly associated with the siege in the 1990s has plenty more to offer. Join us in a journey through the past where the guide will reveal the small and big secrets of this city and introduce you to the mysticism of Sarajevo streets and alleys. The tour will take you to the main sights of the Ottoman and Austro-Hungarian architecture and will show you a unique line where East meets West. Enjoy the scent of fresh made coffee, flavours from hookah, sound of church bells and call from the minarets. Learn about the assassination that triggered the First World War and discover why Sarajevo is called European Jerusalem.Price: 12 EUR\n\nTime: 10.30 am (duration: 3 hours)\nLocation: Insider agency\nBooking (hour in advance)",
            "https://docs.google.com/forms/d/e/1FAIpQLSf6_c98mdFlX2lERfBEMPdBE9gNJ_bNhI5IE_tjLRUbdKbyeQ/viewform?usp=sf_link",
            "vijecnica",
            "1.4km",
            "3h",
            R.drawable.bg_guide_grand,
            arrayOf(1126, 1049, 14, 1689, 1123, 1219, 172, 88, 1051),
            arrayOf("0.0", "0.1", "0.3", "0.1", "0.1", "0.05", "0.1", "0.3", "0.2"),
            true
    ))
    list.add(Guide(
            "TUNNEL TOUR",
            "This tour is a minimum of what you should know about one of the longest siege in modern human history. During the bus ride, the guide will explain the sites and buildings you see and who were important during the siege of Sarajevo 1992 - 1995. Find out how the war started, why the National Library of BH was destroyed, how many people died. While driving to the Tunnel Museum along the Sniper Alley, you’ll have the opportunity to hear the stories about the life in the time of darkness yet life full of hope. All this is just an introduction to the story about the importance of an 800 m long and only 1, 60 m high tunnel that connected the occupied and free Sarajevo territory. Upon arrival at the museum, you’ll pass through 20 m of the original tunnel and see the movie about the war in Sarajevo upon which the guide will explain the role of the tunnel during the siege, its construction, use method and will happily answer all your questions. On the way back to the town, let’s talk about Sarajevo and its future.\n\nPrice: 15 EUR per person (2 person minimum)\nTime: 2 pm\nEvery day\nLocation: Insider Agency\nInfo: Camera, water, pre-booking required\nTransportation by mini-bus, duration 2 h, entrance fee to the Tunnel included",
            "https://docs.google.com/forms/d/e/1FAIpQLSf6hHcfZzGzF0l2znmp1_r59VapNm6agmjTtG0WWXLfgW349A/viewform?usp=sf_link",
            "markale",
            "15km",
            "2h",
            R.drawable.bg_guide_tunnel,
            arrayOf(1049, 1221, 350, 5668, 1223, 1248, 1229, 3126, 3539),
            arrayOf("0.0", "1.0", "0.8", "0.5", "1.4", "0.5", "4.0", "1.6", "5.5"),
            true
    ))
    list.add(Guide(
            "EAT, PRAY, LOVE TOUR",
            "Interested in the history, culture and food of the city? This is the tour for you! Meet our Julias in front of the Insider agency and embrace the spirit of Sarajevo in every possible way. Admire the symbol of the Austro-Hungarian rule, hear the story about the Spite House, go through two streets where you’ll learn more about different crafts which have existed in Sarajevo since 15th century. Discover the heart of the Ottoman rule – Sarajevo’s Bascarsija then drink water from Sebilj fountain and come back to Sarajevo again. What’s the difference between “burek” and all other pies? We’ll certainly answer that question. Pray in four big houses of God in Sarajevo and find out why people call it a “European Jerusalem”. Learn about the hospitality over a cup of Bosnian coffee and share the importance and the meaning of its drinking. See the line between two worlds – Eastern and Western. Finally, try “baklava”! Believe us; it’ll definitely make your day.\n\nPrice: 30 EUR per person (2 person minimum)\nTime: 10 am\nEvery day\nLocation: Insider Agency\nInfo: Empty stomach, comfortable clothes and shoes, sun glasses, camera, pre-booking required\nlight walk, duration 4h",
            "https://docs.google.com/forms/d/e/1FAIpQLSdWOxJ6PQGz_avNku9-nHqeOnVTJB-E0lcr6j26WBGVgU5UfA/viewform?usp=sf_link",
            "sebilj",
            "1.5km",
            "4h",
            R.drawable.bg_guide_pray,
            arrayOf(1126, 1049, 14, 1123, 1219, 174, 3533, 172, 88, 1051),
            arrayOf("0.0", "0.1", "0.3", "0.2", "0.05", "0.2", "0.1", "0.2", "0.3", "0.2"),
            true
    ))
    list.add(Guide(
            "TIMES OF MISFORTUNE",
            "This has been our most popular tour for the last couple of years. During this tour we will try to show you everything that Sarajevans lived through from 1992-1995. At the beginning, we drive to one of the most attractive panoramas of the city. From this place we will explain the siege of Sarajevo. We’ll visit the cemetery of heroes who unarmed stood for a defence of this town and gave their own lives for its freedom. Find out what it was like to live without food, water, electricity, gas, how the children were going to school and what was the cultural and the sports life. Try to understand the reason for shelling cemeteries, Olympic buildings, the Maternity...On this tour you’ll often hear the personal experiences of our guides which will be really something special. Learn more about the collapse of Yugoslavia, the war aims and plans of division of Bosnia and Herzegovina. While driving to the Tunnel Museum along the Sniper Alley, you’ll have the opportunity to hear the stories and comments given by the Insider. Upon arrival at the museum, you’ll pass through 20 m of the original tunnel and see the movie about the war in Sarajevo upon which the guide will explain the role of the tunnel during the siege, its construction, use method and will happily answer all your questions. Find out more about the role of UN forces in Sarajevo and their numerous mistakes committed in this war. On the way back to the town we drive through another entity, talk about the end of war, organization of the state and the current situation. This tour encompass second panorama and visit to another place from which the aggressor was terrorizing the inhabitants of Sarajevo.\n\nPrice: 27 EUR per person\nTime: 11 am\nEvery day\nLocation: Insider Agency\nInfo: camera, water, pre-booking required\ntransportation by mini-bus, duration 3 h, entrance fee to the Tunnel included",
            "https://docs.google.com/forms/d/e/1FAIpQLSdXbdqQ_0IASIZkxeABNWGNAmpliQ_d5azIyl9YWQvtIS5lyQ/viewform?usp=sf_link",
            "zemaljski",
            "30km",
            "3h",
            R.drawable.bg_guide_misfortune,
            arrayOf(1161, 1687, 2460, 350, 1508, 1223, 1248, 1229, 3126, 3539),
            arrayOf("0.0", "1.3", "1.1", "0.8", "1.9", "4.3", "0.5", "4.0", "1.6", "5.5"),
            false
    ))
    list.add(Guide(
            "FREE TOUR",
            "First time in Sarajevo? Want to discover sights that are not covered by other tours? Our Free Tour is the right choice! Join us in an insightful, fun-filled and informative walk about history, society, architecture and what Bosnians are actually like. Discover and experience our city through the eyes of the Insiders who will bring to life the city's past and present. You can decide how much what you saw and heard was worth so your tip will be the only reimbursement to the Insider’s guide. Nonetheless, your smile and happy face will be the greatest compliment to our work.\n\nPrice: FREE\nTime: 4:30 pm\nEvery day\nLocation: Insider Agency\nInfo: camera, water, good mood\nlight walk, 1.5 h",
            "https://docs.google.com/forms/d/e/1FAIpQLSfmrEyQsS1qB4RzsXcL6v8ZfYiaOuhdUrjJ02T5y9hKfGBtFQ/viewform?usp=sf_link",
            "begova",
            "2km",
            "1.5h",
            R.drawable.bg_guide_free,
            arrayOf(189, 1667, 1049, 14, 1123, 1235, 3533, 172, 88, 1051),
            arrayOf("0.0", "0.1", "0.3", "0.3", "0.2", "0.05", "0.05", "0.5", "0.3", "0.2"),
            true
    ))
    list.add(Guide(
            "SARAJEVO ASSASSINATION TOUR",
            "Sarajevo assassination is one of the three things that marked Sarajevo's history. Join us in a tour that will help you understand reasons for the beginning of WWI, the political climate in Europe before 1914 and how Sarajevo Assassination changed the course of world’s history. The guide will take you through the main sites of the Austro-Hungarian architecture, explain their influence on Bosnia and Herzegovina and talk about the impact that the Assassination had on the whole world. Find out what happened with the members of Young Bosnia and how Gavrilo Princip is perceived today.\n\nPrice: 19 EUR per person\nTime: 10.30 am\nEvery day\nLocation: Insider Agency\nInfo: camera, water\nlight walk, 3h",
            "https://docs.google.com/forms/d/e/1FAIpQLSche4TvnciPmWhwd--Q35wfIdVhL8ZRB7GF6G5oeELOecmvxg/viewform?usp=sf_link",
            "galerijabih",
            "1.6km",
            "3h",
            R.drawable.bg_guide_assasin,
            arrayOf(1563, 88, 1221, 171, 594, 100, 2491, 174),
            arrayOf("0.0", "0.2", "0.2", "0.2", "0.05", "0.3", "0.5", "0.7"),
            true
    ))
    list.add(Guide(
            "OLYMPIC TOUR",
            "The XIV Olympic Winter Games were held in Sarajevo from February 8th to 19th, 1984. It was the first Winter Olympics to be held in a communist country. Come with us to see the place where opening cerammony took place and discover what the Olympic games meant for people of Sarajevo and how they prepared for it. We will take you to three breathtaking mountains around the city that hosted the Olypmics but some of which are still in ruins from the war. At the end visit the Olympic museum where we proudly keep the memory of one of the most succesfull Winter Olypics of the 20th century.\n\nPrice: 39 EUR per person (2 people minimum)\nTime: 10.00 am\nBooking: An hour in advance\nDuration: 4h",
            "https://docs.google.com/forms/d/e/1FAIpQLSd4EIo4SZVmOm--D-0rm2BNZW7wxfF8s0gPjw7dSHf5-GD-uQ/viewform?usp=sf_link",
            "",
            "70km",
            "4h",
            R.drawable.bg_guide_olympic,
            arrayOf(1508, 3552, 3555, 3554),
            arrayOf("0.0", "20.0", "30.0", "5.0"),
            false
    ))
    list.add(Guide(
            "JEWISH TOUR",
            "The end of XV century in Europe was the end of peaceful life of one people. After the fall of Granada in 1492, began the persecution of Jews from Spain and Portugal. Find out how in the XVI century the Sarajevans got new neighbours – the Sephardim Jews and what was the life look like in Sijavus Pasha’s Quarter. What they used to do, what songs they sung, which specialities they used to prepare, what is the Ladino language?On this tour you can hear about all of this. The tour will tell you the story of the Jewish Community in Sarajevo, the story of Sephardim and Ashkenazim Jews, their life, happiness, suffering, persecution, place of their eternal resting and place of struggle with death and suffering.\n\nPrice: 25 EUR (2 person minimum)\nTime: 10.00 am\nOn request\nLocation:Insider Agency\nComfortable shoes, camera, water, 24 hrs advance booking required\nlight walk, duration 4 h, entrance fee to the Museum of Jews of BH and transportation to Jewish Cemetery included.",
            "https://docs.google.com/forms/d/e/1FAIpQLScynkA7ClFJZlnuz0C8kNNBue3-NmJJ9ra5UF4cDoGhmZ2oOA/viewform?usp=sf_link",
            "jevreji",
            "3.2km",
            "4h",
            R.drawable.bg_guide_jewish,
            arrayOf(1218, 3535, 189, 1049, 14, 172, 1678, 1679),
            arrayOf("0.0", "0.3", "0.5", "0.4", "0.3", "0.4", "0.01", "3.0"),
            true
    ))
    return list
}