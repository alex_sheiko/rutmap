package rutmap.ba.rutmap.util.map.offline;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class OfflineFileManager {

    public static File offlineMap;
    private static ArrayList<File> offlineFiles = new ArrayList<>();
    private static File localFile;

    public static void init(final Context context) {
        localFile = new File(context.getFilesDir() + "/tiles/");
        localFile.mkdirs();
        localFile.mkdir();

        try {
            InputStream inputStream = context.getResources()
                    .getAssets()
                    .open("sarajevo.mbtiles");
            FileOutputStream outputStream = new FileOutputStream(context.getFilesDir() + "/tiles/");

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        offlineMap = localFile;
    }
}
