package rutmap.ba.rutmap.util.design.image

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import rutmap.ba.rutmap.R

fun ImageView.showImage(ctx: Context, image: Any) {
    Glide.with(ctx)
            .load(image)
            .error(R.drawable.bg_location_default)
            .centerCrop()
            .into(this)
}

fun ImageView.showImageFit(ctx: Context, image: Any) {
    Glide.with(ctx)
            .load(image)
            .into(this)
}