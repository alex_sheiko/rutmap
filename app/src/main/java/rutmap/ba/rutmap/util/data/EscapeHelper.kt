package rutmap.ba.rutmap.util.data

fun String.escapeQuotes(): String {
    return escapeSingleQuotes()
}

fun String.escapeSingleQuotes() = replace("\\\'", "’")

fun String.removeSpacesBefore() = replace("' ", "'")