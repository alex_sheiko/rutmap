package rutmap.ba.rutmap.util.design.view

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import java.lang.Integer.parseInt

class ViewPagerMap(ctx: Context, attrs: AttributeSet)
    : ViewPager(ctx, attrs) {

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (tag != null) {
            tag.toString().split(",").forEach {
                if (currentItem == parseInt(it)) {
                    requestDisallowInterceptTouchEvent(true)
                }
            }
        }
        return super.onInterceptTouchEvent(event)
    }
}