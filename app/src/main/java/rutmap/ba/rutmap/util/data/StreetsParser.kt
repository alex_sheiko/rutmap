package rutmap.ba.rutmap.util.data

import com.google.gson.Gson
import org.jetbrains.anko.db.RowParser
import rutmap.ba.rutmap.data.model.Street
import rutmap.ba.rutmap.data.remote.BuildingsResponse
import java.lang.reflect.Type

class StreetsParser(
        val buildingsType: Type,
        val gson: Gson,
        val skipBuildings: Boolean = false) : RowParser<Street> {

    override fun parseRow(columns: Array<Any?>): Street {

        val buildingResponse: BuildingsResponse
        if (skipBuildings) {
            buildingResponse = BuildingsResponse(emptyList())
        } else {
            buildingResponse = gson.fromJson<BuildingsResponse>(columns[7] as String, BuildingsResponse::class.java)
        }
        return Street(
                (columns[0] as Long).toInt(),
                columns[1] as String,
                columns[2] as String,
                columns[3] as String,
                columns[4] as String,
                columns[5] as String,
                columns[6] as String?,
                buildingResponse
        )
    }
}
