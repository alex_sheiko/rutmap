package rutmap.ba.rutmap.util.map.offline;

/**
 * Constants used in application
 */
public class Consts {
    public static final String OVERLAY_SOURCE_NAME = "osm2vectortiles-overlay-source";
    public static final String OVERLAY_LAYER_NAME = "osm2vectortiles-overlay";

    public static final String SCHEME_MBTILES = "mbtiles://";
    public static final String SCHEME_ASSET = "asset://";
}
