package rutmap.ba.rutmap.util.map.offline;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Utilities for copying files from assets and reading assets as text, will be replaced
 */
class FileUtils {

    static String readAssetFile(Context context, String name) {
        StringBuilder fileData = new StringBuilder(1000);
        BufferedReader reader;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open(name)));

            char[] buf = new char[1024];
            int numRead;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
            reader.close();
            return fileData.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Copy file from assets into local storage, temporary solution
    static String copyFile(Context context, String filename) {
        AssetManager assetManager = context.getAssets();
        InputStream in;
        OutputStream out;
        try {
            String newFileName = context.getFilesDir() + "/" + filename;
            File copiedFile = new File(newFileName);
            if (!copiedFile.exists()) {

                Log.e("Copy", "Copying file from assets:" + filename + " => " + newFileName);
                in = assetManager.open(filename);
                out = new FileOutputStream(copiedFile);
                byte[] buffer = new byte[4096];
                int read;
                while ((read = in.read(buffer)) != -1) {
                    out.write(buffer, 0, read);
                }
                in.close();
                out.flush();
                out.close();
            }
            return newFileName;

        } catch (Exception e) {
            Log.e("EXCEPT: ", e.getMessage());
        }
        return filename;
    }

}
