package rutmap.ba.rutmap.util.map

import android.app.Activity
import android.content.Context
import com.mapbox.mapboxsdk.maps.MapboxMap
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.ui.placedetail.PlaceDetailActivity

fun MapboxMap.setMarkerClickListener(
        ctx: Context,
        placeList: List<Place>) {

    markerViewManager.setOnMarkerViewClickListener({ marker, _, _ ->
        val place = placeList.filter {
            val placeSubject = it
            (placeSubject.lat == "${marker.position.latitude}")
                    && (placeSubject.lon == "${marker.position.longitude}")
        }.firstOrNull()

        if (place != null) {
            ctx.startActivity<PlaceDetailActivity>(
                    "place" to place
            )
        } else {
            ctx.startActivity<PlaceDetailActivity>(
                    "placeName" to marker.title
            )
        }
        (ctx as Activity).overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out)
        true
    })
}