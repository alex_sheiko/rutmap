package rutmap.ba.rutmap.util.data.model

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.location.Location
import android.media.MediaPlayer
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mapbox.mapboxsdk.geometry.LatLng
import io.reactivex.Observable
import kotlinx.android.synthetic.main.item_guide_stop.view.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.enabled
import org.jetbrains.anko.longToast
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.*
import rutmap.ba.rutmap.util.common.async
import rutmap.ba.rutmap.util.common.createMediaPlayer
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.data.audioIdSet
import rutmap.ba.rutmap.util.data.getAudioFile
import rutmap.ba.rutmap.util.data.getGuideList
import rutmap.ba.rutmap.util.data.hasAudioDownloaded
import rutmap.ba.rutmap.util.design.getLabelBackground
import rutmap.ba.rutmap.util.design.image.getImageToShow
import rutmap.ba.rutmap.util.design.image.showImage
import java.io.File
import java.lang.Double.parseDouble
import java.lang.Integer.parseInt

var mMediaPlayer: MediaPlayer? = null
var mPlayedFile: File? = null

fun TextView.showName(place: Place) {
    text = context.getLocalized(place, "name")
}

@SuppressLint("SetTextI18n")
fun TextView.showStreet(ctx: Context,
                        place: Place) {
    text = "${place.address} ${place.buildingnumber}"
}

val Place.buildingnumber: String get() = streetNumber.replace("bb", "")

fun TextView.showPlaceCategory(place: Place) {
    place.loadCategory(context)
            .async()
            .subscribe {
                text = context.getLocalized(it, "name")
                showCategoryBackground(it)
            }
}

fun Place.loadCategory(ctx: Context): Observable<Category> {
    return DataManager(ctx).loadPlaceCategory(cat_id)
}

fun Place.loadCategoryAsync(ctx: Context,
                            callback: (Category) -> Unit) {
    DataManager(ctx).loadPlaceCategory(cat_id)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.loadCategoriesAsync(
        placeList: List<Place>? = null,
        callback: (List<Category>) -> Unit) {
    DataManager(ctx).loadCategories(placeList)
            .async()
            .subscribe { callback.invoke(it) }
}

fun TextView.showCategoryBackground(category: Category) {
    (background as GradientDrawable).setColor(category.getLabelBackground())
}

fun TextView.showPlace(ctx: Context,
                       event: Event) {
    event.loadPlaceAsync(ctx, {
        showPlace(it)
    })
}

fun TextView.showPlace(place: Place) {
    text = place.naziv
}

fun Event.loadPlaceAsync(ctx: Context, callback: (Place) -> Unit) {
    DataManager(ctx).loadPlace(objectId)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.loadPlaceAsync(id: Int,
                           callback: (Place) -> Unit) {
    DataManager(this).loadPlace(id)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.loadPlaceAsync(name: String,
                           callback: (Place) -> Unit) {
    DataManager(this).loadPlace(name)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.loadEventAsync(id: Int,
                           callback: (Event) -> Unit) {
    DataManager(this).loadEvent(id)
            .async()
            .subscribe {
                callback.invoke(it)
            }
}

fun Context.loadStreetAsync(id: Int,
                            callback: (Street) -> Unit) {
    DataManager(this).loadStreet(id)
            .async()
            .subscribe { callback.invoke(it) }
}

fun loadGuideAsync(queryId: Int,
                   callback: (Guide) -> Unit) {

    val guide = getGuideList()
            .find { it.getGuideId() == queryId }!!
    callback.invoke(guide)
}

// What can be a unique integer in a guide?
// A sum of two last places in it
fun Guide.getGuideId() = placeArray[placeArray.size - 1] +
        placeArray[placeArray.size - 2]

fun Context.loadPlacesAsync(category: Category? = null,
                            isOnlyPremium: Boolean = true,
                            isAd: Boolean = false,
                            callback: (List<Place>) -> Unit) {
    DataManager(this).loadPlaces(
            category,
            isOnlyPremium,
            isAd = isAd)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.loadPlacesAsync(ids: List<Int>,
                            callback: (List<Place>) -> Unit) {
    DataManager(this).loadPlaces(ids)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.loadPlaceAsync(lat: Double, callback: (Place) -> Unit) {
    DataManager(this).loadPlace(lat)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Place.loadStreetAsync(ctx: Context, callback: (Street) -> Unit) {
    val idInt: Int
    if (street_id != null && street_id.isNotEmpty()) {
        idInt = parseInt(street_id)
    } else {
        idInt = 1
    }
    DataManager(ctx).loadStreet(idInt)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Place.getLocation(): Location {
    val location = Location("")
    if (lat != "NULA" && lat.isNotEmpty()) {
        location.latitude = parseDouble(lat)
        location.longitude = parseDouble(lon)
    } else {
        location.latitude = 0.0
        location.longitude = 0.0
    }
    return location
}

fun Place.getLatLng(): LatLng {
    if (lat != "NULA" && lat.isNotEmpty()) {
        return LatLng(parseDouble(lat), parseDouble(lon))
    } else {
        return LatLng(0.0, 0.0)
    }
}

fun StreetBuilding.getLatLng(): LatLng {
    if (lat != "NULA") {
        return LatLng(
                parseDouble(lat.replace("\\r\\n", "")),
                parseDouble(lng.replace("\\r\\n", "")))
    } else {
        return LatLng(0.0, 0.0)
    }
}

fun Place.isPremium(): Boolean {
    return `package` > 1
}

fun LinearLayout.showGuidePlaces(idArray: Array<Int>,
                                 distanceArray: Array<String>,
                                 buttonAudio: Button) {
    val idList = idArray.toList()
    context.loadPlacesAsync(idList, {
        val placeList = it
        placeList.sortedBy { idArray.indexOf(it.id) }
                .forEachIndexed { index, place ->
                    val distance = distanceArray[index]
                    addView(createView(place, distance))
                }
        placeList.forEach {
            if (it.hasAudioDownloaded(context)) {
                buttonAudio.text = context.getString(R.string.downloaded)
            }
        }
        var hasNoAudio = true
        placeList.forEach {
            if (it.hasAudio()) {
                hasNoAudio = false
            }
        }
        if (hasNoAudio) {
            buttonAudio.text = context.getString(R.string.no_audio)
            buttonAudio.enabled = false
            buttonAudio.alpha = 0.3f
        }
    })
}

private fun LinearLayout.createView(place: Place, distance: String): View {
    val inflater = LayoutInflater.from(context)
    val view = inflater.inflate(R.layout.item_guide_stop, this, false)
    view.showPlaceInfo(childCount + 1, place, distance)
    return view
}

private fun View.showPlaceInfo(index: Int,
                               place: Place,
                               distance: String) {
    textName.showName(place)
    textDesc.showStreet(context, place)
    textIndex.text = "$index"

    if (place.hasAudio()) {
        buttonPlay.visibility = VISIBLE
        buttonPlay.onClick {
            if (buttonPlay.isChecked) {
                if (place.hasAudioDownloaded(context)) {
                    val selectedFile = place.getAudioFile(context)
                    if (mMediaPlayer == null || selectedFile != mPlayedFile) {
                        mMediaPlayer = context.createMediaPlayer(selectedFile)
                        mPlayedFile = selectedFile
                    }
                    mMediaPlayer!!.start()
                } else {
                    context.longToast("Download audio files first. Click the button on top.")
                    buttonPlay.isChecked = true
                }
            } else {
                try {
                    mMediaPlayer?.pause()
                } catch (e: IllegalStateException) {
                    e.printStackTrace()
                }
            }
        }
    }
    textDistance.text = "$distance\nkm"
}

fun stopAudio() {
    try {
        mMediaPlayer?.stop()
        mMediaPlayer = null
    } catch (e: IllegalStateException) {
        e.printStackTrace()
    }
}

fun Place.hasAudio(): Boolean {
    return audioIdSet.contains(id)
}

fun ImageView.showPlaceImage(ctx: Context, slider: Slider) {
    showImage(ctx, slider.getImageToShow())
}

val Place.formattedDescription: Spanned
    get() {
        return Html.fromHtml(description.replace("\\", "").replace("NULA", "No description"))
    }
