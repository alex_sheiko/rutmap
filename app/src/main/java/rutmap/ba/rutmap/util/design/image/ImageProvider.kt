package rutmap.ba.rutmap.util.design.image

import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Slider
import rutmap.ba.rutmap.util.common.Constants.ROOT_URL

fun Event.getImageToShow(): Any {
    if (hasImage()) {
        return getRelatedImage()
    } else {
        return getPlaceholderImage()
    }
}

fun Slider.getImageToShow(): Any {
    return ROOT_URL + image
}

fun Event.hasImage(): Boolean {
    return !mainPic.contains("Default")
            && mainPic.isNotEmpty()
}

fun Event.getRelatedImage(): String {
    if (bigimg != null && bigimg.isNotEmpty()) {
        return ROOT_URL + bigimg
    }
    return ROOT_URL + mainPic
}

fun getPlaceholderImage(): Int {
    return R.drawable.bg_location_default
}