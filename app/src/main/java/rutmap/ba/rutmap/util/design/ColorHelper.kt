package rutmap.ba.rutmap.util.design

import android.graphics.Color.RED
import android.graphics.Color.parseColor
import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.data.model.EventCategory
import rutmap.ba.rutmap.util.data.model.getColorKey

fun EventCategory.getLabelBackground(): Int {
    when (id) {
        1 -> return parseColor("#7fcb1d")
        2 -> return parseColor("#1dcb4a")
        3 -> return parseColor("#cb901d")
        4 -> return parseColor("#04ade4")
        5 -> return parseColor("#e31a53")
        else -> return RED
    }
}

fun Category.getLabelBackground(): Int {
    when (getColorKey()) {
        "A" -> return parseColor("#26A69A")
        "B" -> return parseColor("#7E57C2")
        "C" -> return parseColor("#e6c03c")
        "E" -> return parseColor("#ef5350")
        "F" -> return parseColor("#e07490")
        "G" -> return parseColor("#FFA726")
        "H" -> return parseColor("#66BB6A")
        "M" -> return parseColor("#FFCA28")
        "R" -> return parseColor("#65d0f8")
        "P" -> return parseColor("#EC407A")
        "T" -> return parseColor("#AB47BC")
        "S" -> return parseColor("#FF7043")
        "W" -> return parseColor("#5C6BC0")
        "V" -> return parseColor("#D4E157")
        else -> return parseColor("#78909C")
    }
}