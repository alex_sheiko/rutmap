package rutmap.ba.rutmap.util.data

import android.content.Context
import android.location.Location
import android.preference.PreferenceManager.getDefaultSharedPreferences
import android.support.v4.app.Fragment

val Context.prefs get() = getDefaultSharedPreferences(this)

fun Context.getLocale(): String {
    return prefs.getString("locale", "en")
}

fun Context.saveLocale(lang: String) {
    prefs.edit()
            .putString("locale", lang)
            .apply()
}

fun Context.getSavedLocation(): Location? {
    val location = Location("")
    val lat = prefs.getFloat("myLat", 0f)
    val lng = prefs.getFloat("myLng", 0f)
    if (lat != 0f) {
        location.latitude = lat.toDouble()
        location.longitude = lng.toDouble()
        return location
    } else {
        return null
    }
}

fun Context.saveLocation(location: Location) {
    prefs.edit()
            .putFloat("myLat", location.latitude.toFloat())
            .putFloat("myLng", location.longitude.toFloat())
            .apply()
}

fun Context.firstLaunch(): Boolean {
    return prefs.getBoolean("firstLaunch", true)
}

fun Context.setUserNotNew() {
    prefs.edit()
            .putBoolean("firstLaunch", false)
            .apply()
}

fun Fragment.isMyLocationEnabled(): Boolean {
    try {
        return context.prefs.getBoolean("myLocationEnabled", false)
    } catch (e: Exception) {
        e.printStackTrace()
        return false
    }
}

fun Fragment.saveMyLocationEnabled(isEnabled: Boolean) {
    context.prefs.edit()
            .putBoolean("myLocationEnabled", isEnabled)
            .apply()
}