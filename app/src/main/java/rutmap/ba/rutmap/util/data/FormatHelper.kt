package rutmap.ba.rutmap.util.data

fun String.cleanGarbage(): String {
    return replace("\\", "")
            .replace("&", " & ").replace("  &  ", " & ")
}