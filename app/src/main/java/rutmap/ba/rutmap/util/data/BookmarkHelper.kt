package rutmap.ba.rutmap.util.data

import android.content.Context
import android.view.View
import android.widget.ImageButton
import org.jetbrains.anko.find
import org.jetbrains.anko.onClick
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Bookmark
import rutmap.ba.rutmap.util.common.async

fun toggleBookmark(ctx: Context,
                   isChecked: Boolean,
                   objectId: Int,
                   type: String) {
    val bookmark = Bookmark(objectId, type)

    if (isChecked) {
        ctx.saveBookmark(bookmark)
    } else {
        ctx.deleteBookmark(bookmark)
    }
}

fun Context.isBookmarked(objectId: Int,
                         callback: (Boolean) -> Unit) {
    DataManager(this)
            .isBookmarked(objectId)
            .async()
            .subscribe { callback.invoke(it) }
}

fun Context.saveBookmark(bookmark: Bookmark) {
    DataManager(this).saveBookmark(bookmark)
}

fun Context.deleteBookmark(bookmark: Bookmark) {
    DataManager(this).deleteBookmark(bookmark)
}

fun ImageButton.setCheckState(isChecked: Boolean) {
    if (isChecked) {
        setImageResource(R.drawable.ic_favorite_white_24dp)
    } else {
        setImageResource(R.drawable.ic_favorite_border_white_24dp)
    }
}

fun View.setBookmarkListener(objectId: Int, type: String) {
    val buttonBookmark = find<ImageButton>(R.id.buttonBookmark)
    var isChecked: Boolean

    context.isBookmarked(objectId, {
        isChecked = it
        buttonBookmark.setCheckState(isChecked)
    })
    buttonBookmark.onClick {
        context.isBookmarked(objectId, {
            isChecked = it
            isChecked = !isChecked
            buttonBookmark.setCheckState(isChecked)

            toggleBookmark(context, isChecked, objectId, type)
        })
    }
}