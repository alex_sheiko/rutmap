package rutmap.ba.rutmap.util.data.model

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.text.Html.fromHtml
import android.text.Spanned
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.schedulers.Schedulers.io
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.EventCategory
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.data.model.Slider
import rutmap.ba.rutmap.util.common.Constants.ROOT_URL
import rutmap.ba.rutmap.util.common.getLocalized
import rutmap.ba.rutmap.util.design.getLabelBackground
import rutmap.ba.rutmap.util.design.image.*
import rutmap.ba.rutmap.util.design.setDrawableRight

fun ImageView.showEventImage(ctx: Context, event: Event) {
    showImage(ctx, event.getImageToShow())
}

fun ImageView.showSlider(ctx: Context, slider: Slider) {
    showImageFit(ctx, slider.getImageToShow())
}

fun TextView.showEventCategoryBackground(category: EventCategory) {
    (background as GradientDrawable).setColor(category.getLabelBackground())
}

fun TextView.showEventCategory(event: Event) {
    DataManager(context).loadEventCategory(event.catId)
            .subscribeOn(io())
            .observeOn(mainThread())
            .subscribe {
                val category = it
                text = context.getLocalized(category, "name")
                setDrawableRight(loadDrawable(context,
                        event.getCategoryIcon()))
                showEventCategoryBackground(it)
            }
}

fun Place.loadSliderAsync(ctx: Context, callback: (Slider) -> Unit) {
    DataManager(ctx).loadSlider(id)
            .subscribeOn(io())
            .observeOn(mainThread())
            .subscribe { callback.invoke(it) }
}

fun WebView.showStreetView(place: Place) {
    loadUrl(place.composePanoramaUrl())
}

fun Place.composePanoramaUrl(): String {
    return "$ROOT_URL/iframe/$alias-3"
}

val Event.formattedDescription: Spanned get() {
    if (details != null) {
        return fromHtml(details.replaceFirst("<p>$title\n</p>", ""))
    } else {
        return fromHtml("No description")
    }
}

fun formatDesc(desc: String?, title: String): Spanned {
    if (desc != null) {
        return fromHtml(desc.replaceFirst("<p>$title\n</p>", ""))
    } else {
        return fromHtml("No description")
    }
}