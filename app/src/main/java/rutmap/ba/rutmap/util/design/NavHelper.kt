package rutmap.ba.rutmap.util.design

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v4.content.res.ResourcesCompat.getDrawable
import android.support.v4.view.GravityCompat.START
import android.support.v7.app.ActionBarDrawerToggle
import android.view.View.OnClickListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_toolbar_main.*
import kotlinx.android.synthetic.main.nav_main.*
import org.jetbrains.anko.email
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.ui.intro.InfoActivity
import rutmap.ba.rutmap.ui.main.MainActivity
import rutmap.ba.rutmap.util.common.comingSoon
import rutmap.ba.rutmap.util.common.getAppVersion
import rutmap.ba.rutmap.util.design.image.showImage

fun MainActivity.setupSlidingMenu() {
    setupToggle()
    setClickListeners()

    showBackground()
}

private fun MainActivity.showBackground() {
    navBackground.showImage(this, R.drawable.nav_bg)
}

private fun MainActivity.setClickListeners() {
    buttonHome.onClick { close() }
    buttonInfo.onClick { startActivity<InfoActivity>() }
    buttonPhone.onClick { dial(getString(R.string.contact_phone)) }
    buttonEmail.onClick { email(getString(R.string.contact_email)) }
    buttonVersion.onClick { comingSoon() }
    buttonVersion.text = buttonVersion.text.toString().replace("2.0", getAppVersion())
}

fun Activity.dial(number: String) {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:$number")
    startActivity(intent)
}

private fun MainActivity.close() {
    drawer.closeDrawer(START)
}

private fun MainActivity.setupToggle() {
    val toggle = ActionBarDrawerToggle(this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close)

    setToggleIcon(toggle)

    drawer.setDrawerListener(toggle)
    toggle.syncState()
}

private fun MainActivity.setToggleIcon(toggle: ActionBarDrawerToggle) {
    val drawable = getDrawable(
            resources,
            R.drawable.ic_menu_blue,
            theme)

    toggle.isDrawerIndicatorEnabled = false
    toggle.setHomeAsUpIndicator(drawable)
    toggle.toolbarNavigationClickListener = OnClickListener {
        if (drawer.isDrawerVisible(START)) {
            drawer.closeDrawer(START)
        } else {
            drawer.openDrawer(START)
        }
    }
}