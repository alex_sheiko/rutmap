package rutmap.ba.rutmap.util.design.image

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.res.ResourcesCompat
import android.widget.ImageView
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Category
import rutmap.ba.rutmap.data.model.Event
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.util.data.model.loadCategoryAsync

fun loadDrawable(ctx: Context, imageId: Int): Drawable {
    return ResourcesCompat.getDrawable(ctx.resources, imageId, null)!!
}

fun Category.getIcon(): Int {
    when (nameEn) {
        "FOOD AND DRINKS" -> return R.drawable.ic_category_food
        "NIGHT LIFE" -> return R.drawable.ic_category_night
        "EDUCATION" -> return R.drawable.ic_category_education
        "Accommodation" -> return R.drawable.ic_category_housing
        "TOURISM AND TRANSPORT" -> return R.drawable.ic_category_transport
        "SHOPS AND SERVICES" -> return R.drawable.ic_category_services
        "ARTS AND ENTERTAINMENT" -> return R.drawable.ic_category_fun
        "Parks and Attractions" -> return R.drawable.ic_category_attractions
        else -> return R.drawable.ic_all_categories
    }
}

fun Event.getCategoryIcon(): Int {
    when (catId) {
        1 -> return R.drawable.ic_events_movie
        2 -> return R.drawable.ic_events_sport
        3 -> return R.drawable.ic_events_theater
        4 -> return R.drawable.ic_events_music
        else -> return R.drawable.ic_events_other
    }
}

fun Category?.getPlaceIcon(ctx: Context,
                           premium: Boolean,
                           isAd: Boolean = false): Int {
    if (isAd) {
        return R.drawable.ic_marker_ad_big
    }
    if (this == null) {
        return R.drawable.ic_marker_category_other_places
    }
    val suffix = aliasEn.replace("-", "_")
    val resId: Int
    if (premium) {
        resId = ctx.resources.getIdentifier(
                "ic_marker_category_${suffix}_big", "drawable", ctx.packageName)
        if (resId == 0) {
            return R.drawable.ic_marker_category_other_places_big
        }
    } else {
        resId = ctx.resources.getIdentifier(
                "ic_marker_category_$suffix", "drawable", ctx.packageName)
        if (resId == 0) {
            return R.drawable.ic_marker_category_other_places
        }
    }
    return resId
}

fun ImageView.showIcon(ctx: Context, place: Place) {
    place.loadCategoryAsync(ctx, {
        val icon = it.getPlaceIcon(ctx, true)
        setImageResource(icon)
    })
}