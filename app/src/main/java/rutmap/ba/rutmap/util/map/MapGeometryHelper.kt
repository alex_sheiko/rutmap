package rutmap.ba.rutmap.util.map

import android.content.Context
import android.graphics.Color.parseColor
import android.location.Location
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.MarkerView
import com.mapbox.mapboxsdk.annotations.MarkerViewOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newCameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory.newLatLngBounds
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapboxMap
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.DataManager
import rutmap.ba.rutmap.data.model.*
import rutmap.ba.rutmap.util.common.async
import rutmap.ba.rutmap.util.data.getSavedLocation
import rutmap.ba.rutmap.util.data.model.*
import rutmap.ba.rutmap.util.design.image.getPlaceIcon
import rutmap.ba.rutmap.util.writeOnDrawable

var mPreviousZoom: Double = 0.0

fun MapboxMap.showMarkerDefault(location: Location) {
    val latLng = LatLng(
            location.latitude,
            location.longitude)

    val markerOptions = MarkerViewOptions()
            .position(latLng)

    addMarker(markerOptions)
}

fun createMarkerPlace(ctx: Context,
                      place: Place,
                      category: Category?,
                      isAd: Boolean = false)
        : MarkerViewOptions {
    val iconFactory = IconFactory.getInstance(ctx)
    val icon = iconFactory.fromResource(
            category.getPlaceIcon(ctx, place.isPremium(), isAd))

    val markerOptions = MarkerViewOptions()
            .title(place.naziv)
            .position(place.getLatLng())
            .icon(icon)

    return markerOptions
}

fun MapboxMap.showMarkerStreet(ctx: Context,
                               building: StreetBuilding): MarkerView {
    val iconFactory = IconFactory.getInstance(ctx)

    val bitmap = ctx.writeOnDrawable(
            R.drawable.ic_marker_street_building,
            building.streetnumber)

    val icon = iconFactory.fromBitmap(bitmap)
    val marginRight = icon.bitmap.width / 2

    val markerViewOptions = MarkerViewOptions()
            .position(building.getLatLng())
            .title("${building.street} ${building.streetnumber}")
            .icon(icon)

    val marker = addMarker(markerViewOptions)
    marker.setRightOffsetPixels(marginRight)
    return marker
}

fun MapboxMap.showMarkerGuide(ctx: Context,
                              index: Int,
                              place: Place): MarkerView {
    val iconFactory = IconFactory.getInstance(ctx)

    val bitmap = ctx.writeOnDrawable(
            R.drawable.ic_marker_guide_place,
            "$index",
            parseColor("#106e50"))

    val icon = iconFactory.fromBitmap(bitmap)
    val marginRight = icon.bitmap.width / 2

    val markerViewOptions = MarkerViewOptions()
            .position(place.getLatLng())
            .title(place.naziv)
            .icon(icon)

    val marker = addMarker(markerViewOptions)
    marker.setRightOffsetPixels(marginRight)
    return marker
}

fun MapboxMap.moveToBounds(bounds: LatLngBounds, ctx: Context) {
    moveCamera(newLatLngBounds(bounds, 64.dpToFixels(ctx)))
}

fun calculateBounds(locationList: List<Location>): LatLngBounds {
    val latLngList = ArrayList<LatLng>()

    locationList.forEach {
        latLngList.add(it.toLatLng())
    }
    // Prevent crash for streets with only one building
    if (latLngList.size == 1) {
        latLngList.add(latLngList[0])
    }
    return LatLngBounds.Builder()
            .includes(latLngList)
            .build()
}

fun Location.toLatLng(): LatLng {
    return LatLng(latitude, longitude)
}

fun LatLng.toLocation(): Location {
    val location = Location("")
    location.latitude = latitude
    location.longitude = longitude
    return location
}

fun MapboxMap.showMarkersFamous(ctx: Context,
                                upclose: Boolean = false,
                                callback: ((Unit) -> Unit)? = null) {
    val placesAndIcons = if (upclose)
        getFamousPlaceIdsLargeIcons() else
        getFamousPlaceIds()

    val placeIds = placesAndIcons.keys.toList()
    val markerList = ArrayList<MarkerViewOptions>()

    placeIds.forEach {
        val placeId = it

        ctx.loadPlaceAsync(placeId, {
            val place = it

            val iconId = placesAndIcons[placeId]!!
            val iconFactory = IconFactory.getInstance(ctx)
            val icon = iconFactory.fromResource(iconId)

            val markerOptions = MarkerViewOptions()
                    .title(place.naziv)
                    .position(place.getLatLng())
                    .icon(icon)
            addMarker(markerOptions)
            markerList.add(markerOptions)

            if (markerList.size == placeIds.size) {
                callback?.invoke(Unit)
            }
        })
    }

    /* More optimized, but mixes up icons: fix or remove
    ctx.loadPlacesAsync(placeIds, {
        val markerList = ArrayList<MarkerViewOptions>()
        it.forEachIndexed { index, place ->
            val iconId = iconIds[index]
            val iconFactory = IconFactory.getInstance(ctx)
            val icon = iconFactory.fromResource(iconId)

            val markerOptions = MarkerViewOptions()
                    .position(place.getLatLng())
                    .icon(icon)
            markerList.add(markerOptions)
        }
        if (markerList.size == placeIds.size) {
            addMarkerViews(markerList)
            callback?.invoke(Unit)
        }
    }) */
}

fun MapboxMap.showMarkersPlaces(ctx: Context,
                                parentCategory: Category?,
                                isOnlyPremium: Boolean = true,
                                isRecommended: Boolean = false,
                                isAd: Boolean = false,
                                callback: ((ArrayList<Location>) -> Unit)? = null) {
    ctx.loadPlacesAsync(
            parentCategory,
            isOnlyPremium || isRecommended,
            isAd, {
        val placeList = it

        if (placeList.isNotEmpty()) {
            loadCategoriesShowMarkers(ctx, placeList, isAd, callback)

            setMarkerClickListener(ctx, placeList)
        }
    })

    setRedrawListener(ctx, parentCategory, isRecommended, isAd)
}

private fun MapboxMap.loadCategoriesShowMarkers(ctx: Context,
                                                placeList: List<Place>,
                                                isAd: Boolean = false,
                                                callback: ((ArrayList<Location>) -> Unit)? = null) {
    val markerList = ArrayList<MarkerViewOptions>()
    ctx.loadCategoriesAsync(placeList, {
        val categoryList = it

        val locationList = ArrayList<Location>()

        placeList.forEachIndexed { _, place ->
            val category = categoryList.filter {
                it.id == place.cat_id
            }.firstOrNull()

            val markerOptions = createMarkerPlace(
                    ctx,
                    place,
                    category,
                    isAd)
            markerList.add(markerOptions)

            locationList.add(markerOptions.position.toLocation())
        }
        if (markerList.size == placeList.size) {
            addMarkerViews(markerList)
            callback?.invoke(locationList)
        }
    })
}

fun MapboxMap.showMarkersStreet(ctx: Context,
                                street: Street,
                                callback: (ArrayList<Location>) -> Unit) {
    val locationList = ArrayList<Location>()

    street.points.ulice.forEach {
        val marker = showMarkerStreet(ctx, it)
        locationList.add(marker.position.toLocation())
    }
    callback.invoke(locationList)
}

fun MapboxMap.showMarkersGuide(ctx: Context,
                               guide: Guide,
                               callback: (ArrayList<Location>) -> Unit) {
    val manager = DataManager(ctx)

    manager.loadGuidePlaces(guide)
            .async()
            .subscribe {
                val locationList = ArrayList<Location>()

                val placeList = it
                placeList.sortedBy { guide.placeArray.indexOf(it.id) }
                        .forEachIndexed { index, place ->
                            val marker = showMarkerGuide(ctx, index + 1, place)
                            locationList.add(marker.position.toLocation())
                        }
                callback.invoke(locationList)
            }
}

fun MapboxMap.moveToMyLocation(context: Context) {
    val moveToLocation: Location
    if (myLocation != null) {
        moveToLocation = myLocation!!
    } else {
        moveToLocation = context.getSavedLocation()!!
    }

    val cameraPosition = CameraPosition.Builder()
            .target(LatLng(
                    moveToLocation.latitude,
                    moveToLocation.longitude))
            .build()

    easeCamera(newCameraPosition(cameraPosition))
}