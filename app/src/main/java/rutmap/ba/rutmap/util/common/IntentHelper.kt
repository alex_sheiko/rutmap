package rutmap.ba.rutmap.util.common

import android.app.Activity
import android.support.v4.app.Fragment
import rutmap.ba.rutmap.data.model.*
import rutmap.ba.rutmap.ui.placedetail.PlaceDetailActivity

fun Fragment.getEventExtra(): Event {
    return activity.intent.getParcelableExtra<Event>("event")
}

fun Activity.getPlaceExtra(): Place? {
    try {
        return intent.getParcelableExtra<Place>("place")
    } catch (e: Exception) {
        return null
    }
}

fun Fragment.getPlaceExtra(): Place? {
    if (activity is PlaceDetailActivity && activity.intent.hasExtra("placeName")) {
        return (activity as PlaceDetailActivity).place
    }
    return activity.getPlaceExtra()
}

fun Activity.getCategoryExtra(): Category? {
    try {
        return intent.getParcelableExtra<Category>("category")
    } catch (e: Exception) {
        return null
    }
}

fun Fragment.getCategoryExtra(): Category? {
    return activity.getCategoryExtra()
}

fun Activity.getStreetExtra(): Street {
    return intent.getParcelableExtra<Street>("street")
}

fun Fragment.getStreetExtra(): Street {
    return activity.getStreetExtra()
}

fun Activity.getGuideExtra(): Guide {
    return intent.getParcelableExtra<Guide>("guide")
}

fun Fragment.getGuideExtra(): Guide {
    return activity.intent.getParcelableExtra<Guide>("guide")
}

fun Fragment.eventMode(): Boolean {
    return activity.intent.hasExtra("event")
}