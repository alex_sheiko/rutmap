package rutmap.ba.rutmap.util.design

import android.text.SpannableStringBuilder
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.TextView.BufferType.SPANNABLE
import kotlinx.android.synthetic.main.include_date_selector.*
import org.jetbrains.anko.find
import rutmap.ba.rutmap.ui.eventlist.EventListActivity
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan
import uk.co.chrisjenx.calligraphy.TypefaceUtils.load
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.DAY_OF_YEAR
import java.util.Calendar.getInstance
import java.util.Locale.getDefault

fun EventListActivity.setupDateSelector() {
    getHeaders().showDaysOfWeek()
    getRadioButtons().showDates()

    datesGroup.setupClickListener(this)
}

private fun RadioGroup.setupClickListener(activity: EventListActivity) {
    setOnCheckedChangeListener { _, checkedId ->
        val checkedButton = find<RadioButton>(checkedId)
        val date = checkedButton.tag as Date

        activity.showForDate(date)
    }
}

fun EventListActivity.getHeaders(): List<TextView> {
    val count = containerHeaders.childCount
    val headerList = ArrayList<TextView>()

    headerList += (0..count - 1)
            .asSequence()
            .map { containerHeaders.getChildAt(it) }
            .filterIsInstance<TextView>()

    return headerList
}

private fun EventListActivity.getRadioButtons(): List<RadioButton> {
    val count = datesGroup.childCount
    val buttonList = ArrayList<RadioButton>()

    buttonList += (0..count - 1)
            .asSequence()
            .map { datesGroup.getChildAt(it) }
            .filterIsInstance<RadioButton>()

    return buttonList
}

private fun List<TextView>.showDaysOfWeek() {
    val dates = getDates()

    forEachIndexed { index, button ->
        button.parseAndShowDay(dates[index])
    }
}

private fun List<RadioButton>.showDates() {
    val dates = getDates()

    forEachIndexed { index, button ->
        button.parseAndShowDate(dates[index])
    }
}

private fun <T> List<T>.getDates(): List<Date> {
    val startOffset = 0 // today
    val endOffset = size - 1 // number of buttons

    val dates = (startOffset..endOffset)
            .map(::getDateAfterOffset)

    return dates
}

fun getDateAfterOffset(offset: Int): Date {
    val calendar = getInstance()
    calendar.add(DAY_OF_YEAR, offset)
    return calendar.time
}

private fun TextView.parseAndShowDate(date: Date) {
    val dayFormatter = SimpleDateFormat("dd", getDefault())
    val monthFormatter = SimpleDateFormat("MMM", getDefault())

    showDateWithCustomFont(
            dayFormatter.format(date),
            monthFormatter.format(date))
    tag = date
}

private fun TextView.parseAndShowDay(date: Date) {
    val dayFormatter = SimpleDateFormat("E", getDefault())

    text = dayFormatter.format(date)
}

private fun TextView.showDateWithCustomFont(day: String, month: String) {
    val sBuilder = SpannableStringBuilder()
    sBuilder.append(day) // Default font
            .append("\n")
            .append(month.toUpperCase()) // Bold this

    val typefaceSpan = CalligraphyTypefaceSpan(load(context.assets,
            "fonts/Geomanist-Medium.otf"))

    sBuilder.setSpan(typefaceSpan, 3, 6, SPAN_EXCLUSIVE_EXCLUSIVE)
    setText(sBuilder, SPANNABLE)
}