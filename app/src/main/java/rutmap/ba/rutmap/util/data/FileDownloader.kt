package rutmap.ba.rutmap.util.data

import android.app.Activity
import android.content.Context
import com.liulishuo.filedownloader.BaseDownloadTask
import com.liulishuo.filedownloader.FileDownloadQueueSet
import com.liulishuo.filedownloader.FileDownloadSampleListener
import com.liulishuo.filedownloader.FileDownloader
import org.jetbrains.anko.toast
import rutmap.ba.rutmap.R
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.util.common.Constants
import java.io.File

val audioNameSet = listOf("akademija", "alija", "ars", "askenaska", "begova", "bosnjacki", "brusa", "despic", "galerijabih", "jevreji", "katedrala", "markale", "muzej1878", "pozoriste", "saborna", "sahat", "sebilj", "svrzina", "vijecnica", "zemaljski")
val audioIdSet = listOf(1065, 173, 177, 1218, 1123, 70, 169, 176, 171, 72, 1050, 1221, 174, 100, 1051, 1219, 14, 175, 1049, 1223)

fun Activity.downloadAll(callback: () -> Unit) {
    val queueSet = FileDownloadQueueSet(createListener(callback))

    val tasks = ArrayList<BaseDownloadTask>()
    audioNameSet.forEachIndexed { index, alias ->
        val id = audioIdSet[index]
        tasks.add(FileDownloader.getImpl()
                .create(alias.getUrl(this))
                .setPath(id.getPath(this)))
    }
    queueSet.setAutoRetryTimes(1)

    queueSet.downloadSequentially(tasks)
    queueSet.start()
}

fun Activity.createListener(callback: () -> Unit): FileDownloadSampleListener {
    return object : FileDownloadSampleListener() {

        override fun pending(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
            super.pending(task, soFarBytes, totalBytes)
        }

        override fun progress(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
            super.progress(task, soFarBytes, totalBytes)
        }

        override fun error(task: BaseDownloadTask?, e: Throwable?) {
            super.error(task, e)
        }

        override fun connected(task: BaseDownloadTask, etag: String?, isContinue: Boolean, soFarBytes: Int, totalBytes: Int) {
            super.connected(task, etag, isContinue, soFarBytes, totalBytes)
        }

        override fun paused(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
            super.paused(task, soFarBytes, totalBytes)
        }

        override fun completed(task: BaseDownloadTask) {
            super.completed(task)
            val file = File(task.path)
            file.setReadable(true, false)
            if (task.url.contains("zemaljski")) {
                toast(getString(R.string.audio_below))
                callback.invoke()
            }
        }

        override fun warn(task: BaseDownloadTask?) {
            super.warn(task)
        }
    }
}

fun Place.hasAudioDownloaded(context: Context): Boolean {
    val path = id.getPath(context)
    return File(path).exists()
}

fun Place.getAudioFile(context: Context): File {
    val path = id.getPath(context)
    return File(path)
}

private fun String.getFileName(context: Context): String {
    return "${this}_${context.getLocale().replace("bs", "ba")}"
}

private fun Int.getFileName(context: Context): String {
    return "${this}_${context.getLocale().replace("bs", "ba")}"
}

private fun String.getUrl(context: Context): String {
    return "${Constants.ROOT_URL}/pn/audio/${getFileName(context)}.mp3"
}

private fun Int.getPath(context: Context): String {
    return "${context.externalCacheDir}/${getFileName(context)}.mp3"
}