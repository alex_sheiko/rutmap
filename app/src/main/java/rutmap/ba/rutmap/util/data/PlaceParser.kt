package rutmap.ba.rutmap.util.data

import android.database.Cursor
import rutmap.ba.rutmap.data.model.Place

fun Cursor.retrievePlace(): Place {
    try {
        return Place(
                getInt(column("id")),
                getString(column("naziv")) ?: "",
                getString(column("nazivEn")) ?: "",
                getString(column("alias")) ?: "",
                getString(column("description")) ?: "",
                getString(column("descriptionEn")) ?: "",
                getString(column("category")) ?: "",
                getString(column("categoryParent")) ?: "",
                getString(column("categoryEn")) ?: "",
                getString(column("categoryParentEn")) ?: "",
                getInt(column("cat_id")),
                getInt(column("parentCatId")),
                getString(column("coords")) ?: "",
                getString(column("lat")) ?: "",
                getString(column("lon")) ?: "",
                getString(column("centar1")) ?: "",
                getString(column("centar2")) ?: "",
                getString(column("centar3")) ?: "",
                getString(column("centar4")) ?: "",
                getString(column("opSlika")) ?: "",
                getString(column("opImgUrl")) ?: "",
                getString(column("opImgUrl2")) ?: "",
                getString(column("opImgUrl3")) ?: "",
                getString(column("objectClassImg")) ?: "",
                getString(column("telefon")) ?: "",
                getString(column("mail")) ?: "",
                getString(column("web")) ?: "",
                getString(column("slika")) ?: "",
                getString(column("slider")) ?: "",
                getString(column("address")) ?: "",
                getString(column("street_id")) ?: "",
                getString(column("people")) ?: "",
                getString(column("rating")) ?: "",
                getString(column("categoryClass")) ?: "",
                getString(column("vrsta")) ?: "",
                getString(column("streetNumber")) ?: "",
                getString(column("minZoom")) ?: "",
                getInt(column("package")),
                getString(column("showOnSearch")) ?: "",
                getString(column("tags")) ?: "",
                getString(column("tagsEn")) ?: "",
                getString(column("workTime")))
    } catch (e: Exception) {
        e.printStackTrace()
        return createEmptyPlace()
    }
}

private fun Cursor.column(name: String) = getColumnIndexOrThrow(name)