package rutmap.ba.rutmap.util.common

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.support.v4.app.Fragment

fun Fragment.hasNetwork(): Boolean {
    return context.hasNetwork()
}

fun Context.hasNetwork(): Boolean {
    val manager = getSystemService(CONNECTIVITY_SERVICE)
            as ConnectivityManager
    val netInfo = manager.activeNetworkInfo
    return netInfo != null && netInfo.isConnectedOrConnecting
}

fun Context.hasNoNetwork(): Boolean {
    return !hasNetwork()
}