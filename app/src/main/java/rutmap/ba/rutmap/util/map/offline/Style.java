package rutmap.ba.rutmap.util.map.offline;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Iterator;


public class Style {

    private static final String MAIN_SOURCE = "openmaptiles";
    private JSONObject style;
    private JSONObject sources;
    private JSONArray layers;
    private LatLngBounds overlayBounds;
    private String attribString;
    private Context context;


    public Style(Context context, String json) {
        this.context = context;
        if (json == null) {
            json = "";
        }
        json = json.replace("$$KEY$$", "");
        try {
            style = new JSONObject(json);
            sources = style.getJSONObject("sources");
            layers = style.getJSONArray("layers");
            overlayBounds = null;


            processSources();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Style createFromAsset(Context context, String name) {
        return new Style(context, FileUtils.readAssetFile(context, name));
    }

    public String getAttribString() {
        return attribString;
    }

    /**
     * @return Json representation of this style
     */
    public String getStyleJson() {
        return style.toString();
    }


    /**
     * @return Bounds of shown overlay
     */
    public LatLngBounds getOverlayBounds() {
        return overlayBounds;
    }

    /**
     * Changes shown overlay
     * @param overlayFile
     */
    public void setOverlay(File overlayFile) {

        String format = "png";
        SQLiteDatabase db = SQLiteDatabase.openDatabase(overlayFile.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
        Cursor cursor = db.query("metadata", new String[]{"name", "value"}, null, null, null, null, null);
        cursor.moveToFirst();
        do {
            String metadataName = cursor.getString(0);
            if (metadataName.equals("bounds")) {
                String val = cursor.getString(1);
                String[] parts = val.split(",");
                // lat/lng are swapped
                LatLngBounds.Builder builder = new LatLngBounds.Builder()
                        .include(new LatLng(Double.parseDouble(parts[1]), Double.parseDouble(parts[0])))
                        .include(new LatLng(Double.parseDouble(parts[3]), Double.parseDouble(parts[2])));
                overlayBounds = builder.build();
            }
            if (metadataName.equals("format")) {
                format = cursor.getString(1);
            }
            if (metadataName.equals("attribution")) {
                attribString = cursor.getString(1);
            }
        } while (cursor.moveToNext());
        cursor.close();
        db.close();

        if (format.equals("png") || format.equals("jpg")) {
            // Raster tiles
            try {
                /*
                style = new JSONObject(FileUtils.readAssetFile(context, "style.json"));
                sources = style.getJSONObject("sources");
                layers = style.getJSONArray("layers");
                */

                JSONObject overlaySource = new JSONObject();
                if (sources != null) {
                    sources.put(Consts.OVERLAY_SOURCE_NAME, overlaySource);
                }

                JSONObject overlayLayer = new JSONObject();
                if (layers != null) {
                    layers.put(overlayLayer);
                }

                overlayLayer.put("id", Consts.OVERLAY_LAYER_NAME);
                overlayLayer.put("source", Consts.OVERLAY_SOURCE_NAME);
                overlayLayer.put("type", "raster");

                overlaySource.put("url", Consts.SCHEME_MBTILES + overlayFile.getAbsolutePath());
                overlaySource.put("type", "raster");
                overlaySource.put("tileSize", 256);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            // Ofline vector tiles, just use base style and modify sources
            try {
                JSONObject mapboxSource = sources.getJSONObject(MAIN_SOURCE);
                mapboxSource.put("url", Consts.SCHEME_MBTILES + overlayFile.getAbsolutePath());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        processSources();
    }

    /**
     * Traverses through style json, and copies all mbtiles from assets into local storage
     */
    private void processSources() {
        try {

            Iterator<String> sourceNames = sources.keys();
            while (sourceNames.hasNext()) {
                String name = sourceNames.next();
                JSONObject source = sources.getJSONObject(name);
                String path = source.getString("url");
                if (path.startsWith(Consts.SCHEME_MBTILES)) {
                    String sourceFile = path.substring(Consts.SCHEME_MBTILES.length());
                    String destFile = FileUtils.copyFile(context, sourceFile);
                    source.put("url", Consts.SCHEME_MBTILES + destFile);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
