package rutmap.ba.rutmap.util.common

import android.app.Activity
import android.content.Context
import rutmap.ba.rutmap.data.model.*
import rutmap.ba.rutmap.util.data.cleanGarbage
import rutmap.ba.rutmap.util.data.getLocale
import java.util.*

fun Context.getLocalized(obj: Any, key: String): String {
    if (obj is Event) {
        when (key) {
            "name" -> {
                if (english()) {
                    return obj.titleEn
                } else {
                    return obj.title
                }
            }
            "details" -> {
                if (english()) {
                    return obj.detailsEn ?: ""
                } else {
                    return obj.details ?: ""
                }
            }
        }
    } else if (obj is Place) {
        when (key) {
            "name" -> {
                if (english()) {
                    if (obj.nazivEn.equals("nula", true) || obj.nazivEn.isEmpty()) {
                        return obj.naziv.cleanGarbage()
                    }
                    return obj.nazivEn
                } else {
                    return obj.naziv
                }
            }
            "tags" -> {
                if (english()) {
                    return obj.tagsEn
                } else {
                    return obj.tags
                }
            }
        }
    } else if (obj is Street) {
        when (key) {
            "name" -> {
                return obj.name
            }
        }
    } else if (obj is Category) {
        when (key) {
            "name" -> {
                if (english()) {
                    return obj.nameEn
                } else {
                    return obj.name
                }
            }
        }
    } else if (obj is EventCategory) {
        when (key) {
            "name" -> {
                if (english()) {
                    return obj.titleEn
                } else {
                    return obj.title
                }
            }
        }
    }
    return ""
}

fun Context.english() = getLocale() == "en"

fun Activity.setLocale(lang: String) {
    val locale = Locale(lang)
    Locale.setDefault(locale)
    val config = baseContext.resources.configuration
    config.locale = locale
    baseContext.resources.updateConfiguration(config,
            baseContext.resources.displayMetrics)
}