package rutmap.ba.rutmap.util.common

import android.content.Context
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.applyDimension

fun Context.spToPixels(sp: Int): Float {
    val density = resources.displayMetrics.scaledDensity
    return sp * density
}

fun Context.dpToPixels(dp: Int): Int {
    return applyDimension(COMPLEX_UNIT_DIP, dp.toFloat(),
            resources.displayMetrics).toInt()
}