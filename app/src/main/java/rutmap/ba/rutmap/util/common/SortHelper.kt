package rutmap.ba.rutmap.util.common

import android.location.Location
import rutmap.ba.rutmap.data.model.Place
import rutmap.ba.rutmap.util.data.model.getLocation
import rutmap.ba.rutmap.util.map.getDistanceTo
import java.util.*

fun List<Place>.sortByDistance(myLocation: Location) {
    Collections.sort(this,
            { place1, place2 ->
                val distance1 = place1.getLocation().getDistanceTo(myLocation)
                val distance2 = place2.getLocation().getDistanceTo(myLocation)
                distance1.compareTo(distance2)
            })
}